#!/usr/bin/env python

# Reads2Barcode
# input: mapped reads either via CCS2+barcoding pipeline 41 (PacBio) or PacBioQC script (our implementation) assigned reads
# output: outputs a single cleaned consensus barcode per well



import sys, os, sqlite3
import pandas as pd
from subprocess import call #system calls
import linecache   #to read specific line of the file
import argparse
from openpyxl import load_workbook   # pip install openpyxl
import csv
import re #exact string matching
import tre #fuzzy regex matching
#from subprocess import call
import collections
from Bio.Seq import Seq
from Bio import SeqIO, Entrez, Alphabet
Entrez.email = 'kbessono@uoguelph.ca'
#import copy
import time
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
import random, string #for random number generator
from ete3 import NCBITaxa #get taxonomy given taxID  (e.g. print  NCBITaxa().get_rank(NCBITaxa().get_taxid_translator(NCBITaxa().get_lineage(1666733)).keys()))
from openpyxl import load_workbook   # pip install openpyxl
import pickle #import binary pre-made objects
import requests #read remote HTML pages
from subprocess import PIPE, Popen, check_output

#from Bio import pairwise2

def read_tab_txt(f):
    with open(f,"rU") as f:
        reader = csv.reader(f, delimiter="\t");
        data=[]
        for read in reader:
            if len(read) > 5: #read from the 5th line (due to header)
                data.append(read)
       # m_tags = {}
       # for key, value in reader:
       #     m_tags[key] = value.rstrip("\n")
    f.close()
    return data

def create_dirs(d1):
    if not os.path.exists(d1):
        os.makedirs(d1);
    #if not os.path.exists(d2):
    #    os.makedirs(d2)

def ace_file_parser(args, cap3_contigs):
    ace_file = args.cap3_output[0] + ".cap.ace"
    print ace_file
    ace_data = open(ace_file, "r").readlines()
    #print len(ace_data); print ace_data[0]; sys.exit("Hi")
    contig_dict_counts = {}; set_key="";
    for line in ace_data:
        if re.search('(CO Contig){1,1}', line):
            print line
            key=re.match(r'.*(Contig\d+){1,1}',line).groups(0)[0]
            if not any([k == key for k in contig_dict_counts.keys()]):
                set_key = key;
                contig_dict_counts[set_key]=0;
        if re.search('(AF)', line):
            contig_dict_counts[set_key] += 1
    return contig_dict_counts

def getPrimers(path):
    primers_list=[]
    print("WARNING: Primers are assumed to be ordered FORWARD followed by REVERSE"); time.sleep(5);
    with open(path, "rU") as fp:
        for record in SeqIO.parse(fp, "fasta"):
            primers_list.append(str((record.seq).rstrip("-")))
    if len(primers_list) > 2:
        sys.exit("ERROR: More than 2 primer sequences supplied")
    return primers_list[0], primers_list[1]

def getRegexPrimerStrings(fwd_primer,rev_primer):
    d_dict = {"W": "[A|T]", "S": "[C|G]", "M": "[A|C]", "K": "[G|T]", "R": "[A|G]", "Y": "[C|T]",
              "B": "[C|G|T]", "D": "[A|G|T]", "H": "[A|C|T]", "V": "[A|C|G]", "N": "[A|C|G|T]", "A": "A", "T": "T",
              "G": "G", "C": "C"}  # degenerative dictionary
    m_fwd_pattern = "".join([d_dict[l] for l in fwd_primer])  # forward match
    m_rev_pattern = "".join([d_dict[l] for l in rev_primer])  # reverse match
    return m_fwd_pattern, m_rev_pattern

#step-wise cleaning of the barcode (fwd and then reverse)
def cleanBarcode(fwd_primer_regex,rev_primer_regex,seq):  # orients reads in 5'-3' orientation and clips the COI-5P primers
    #print fwd_primer_regex, rev_primer_regex; cleaned_seq=""
    #print args.notrimming

    if args.notrimming == True:
        return seq #no trimming
    ##seq = "CTGCGTGCTCTACGACGCAGTCGAACATGTAGCTGACTCAGGTCACAGTCAACAAATCATAAAGATATTGGAACTTTATATTTTATTTTCGGCGCTTGGTCAGGAATAATTGGAACATCTTTAAGTATATTAATTCGTGCAGAATTAGGGCACCCGGGAAATTTAATTGGGAATGATCAAATTTATAATGTAATTGTAACTGCTCACGCTTTTATTATGATTTTTTTTATAGTAATACCAATTATAATTGGCGGGTTTGGTAATTGATTGGTCCCTCTGATATTGAGGGCACCTGATATAGCTTTCCCACGAATAAACAATATAAGATTTTGAATACTCCCCCCCTCAATCAGGCTCCTTTTGATTAGTAGCTTAGTTGAAAATGGGGCAGGAACCGGGTGAACAGTTTACCCCCCTCTTTTCAGCCAACATTGCCTCATGCCGGCGGATCAGTTGATTTAGCAATTTTTTCCCTTCATTTAGCTGGAATTTCTTCTATTTTAGGGGCTGTTAATTTTATTACAACTATTATTAACATACGTTCTAATGGAATTACATTTGATCGAATACCTTTATTTGTATGGTCGGTATTTATTACCGCTATTTTATTATTATTATCTTTACCAGTATTGGCTGGGGCAATTACTATACTTTTAACAGATCGAAATATTAATACTTCATTTTTTGACCCTGCCGGAGGGGGAGACCCAATTTTATACCAACATCTATTTTGATTTTTTGGACATCCTGAAGTTTACTACGATGTGATGCTTGCACAAGTGATCCAGCTAGTCGATGACAGCCTACC"
    #FORWARD clip
    idx_start, idx_end = fuzzy_str_regex_match(fwd_primer_regex,seq)
    if idx_start != None and idx_end != None:
        cleaned_seq = seq[idx_end:]
    else:
        seq_rev = str(Seq(seq).reverse_complement())  # reverse complement the read
        idx_start, idx_end = fuzzy_str_regex_match(fwd_primer_regex, seq_rev)
        #print idx_start, idx_end
        if idx_start != None and idx_end != None:
            cleaned_seq = seq_rev[idx_end:]
        else:
            cleaned_seq = seq  #all tries failed, no clipping will be done at this end
    #print  cleaned_seq
    #REVERSE clip
    idx_start, idx_end = fuzzy_str_regex_match(rev_primer_regex, str(Seq(cleaned_seq).reverse_complement()))
    #print idx_start, idx_end
    if idx_start != None and idx_end != None:
        cleaned_seq = cleaned_seq[0:-idx_end]
    else:
        #print idx_start, idx_end
        idx_start, idx_end = fuzzy_str_regex_match(rev_primer_regex, cleaned_seq)
        if idx_start != None and idx_end != None:
            cleaned_seq = cleaned_seq[0:idx_start]
        #else:
        #    print "All clipping methods failed"
            #blast_res = getBlastStatsPerSeq(cleaned_seq, False)
            #if blast_res:
            #    print len(cleaned_seq)
            #    cleaned_seq = cleaned_seq[blast_res[0]["blast_overlap_start_pos"]:blast_res[0]["blast_overlap_end_pos"]]  #if all tries fail, return unclipped sequence

    return cleaned_seq


def fuzzy_str_regex_match(in_pattern, in_str):
    tp = tre.compile(in_pattern)
    fz = tre.Fuzzyness(maxerr=5)
    m = tp.search(in_str, fz)
    if m:
        return m.groups()[0][0], m.groups()[0][1]
    else:
        return None,None

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def getBlastStatsPerSeq(seq, blast_remote_flag, nt = False):
    #print "Do BLASTn and identify OTU"
    print seq, len(seq), blast_remote_flag
    results_dict = {}
    #seq="AACTCTTTATATAATCTTAGGGGTTTGATCTGCTTTTGTGGGGACTTCTCTTAGGGTTTTAATTCGTTTAGAGTTAGGTCAAACTGGTTCTCTTATTGGTAATGATCACATTTATAACGTTATAGTTACCTCTCATGCCTTTATTATGATTTTTTTTATGGTCATGCCTATTCTTATTGGTGGGTTTGGGAATTGATTAGTACCTCTTATATTAGGTGCTCCTGACATAGCTTTTCCTCGCATAAATAATATGAGGTTTTGGCTTCTTCCCCCCTCTTTAGGATTTTTACTTTTTAGTTCTTTTTTGGATTCTGGCTCAGGAACTGGATGGACTGTTTATCCCCCTTTATCTAGTGTTGAGTTTCACAGGGGGTTGTCTGTTGATTTGGTTATTTTTAGTTTGCATTTAGCAGGGGTTTCTTCTATCCTAGGGGCTGTTAATTTTATCACTACTTGTATTAATATGCGTAGAAGTGGTATAATTTGGGATTATGTTCCCCTTTTTGTATGGTCTGTTTTTTTAACTGCTATTTTACTTTTACTTTCCCTGCCTGTACTAGCAGGTGCTATTACAATATTACTAACTGATCGCAATTTAAATACTTCTTTCTTTGATCCCTCTGGTGGGGGAGATCCAATTCTTTATCAGCATCTGTTT"
    tmp_file_name_fasta="tmp"+id_generator()+".fasta"
    tmp_file_results_blast = "tmp" + id_generator() + ".xml"

    if blast_remote_flag == True:
        print "Remote Blast. Submitted query. Waiting ..."
        result_handle = NCBIWWW.qblast(program="blastn", database="nt", sequence=seq, megablast=True)
        fp = open(tmp_file_results_blast, "w"); fp.write(result_handle.read()); fp.close()
        print "Yay! Obtained result!"
    else:
        print "Local Blast"
        fp=open(tmp_file_name_fasta,"w"); fp.write("{!s}\n{!s}".format(">tmp",seq)); fp.close()
        try:
            #parameter references http://etutorials.org/Misc/blast/Part+V+BLAST+Reference/Chapter+13.+NCBI-BLAST+Reference/13.3+blastall+Parameters/

            if nt == False:
                print "blastall -i " + tmp_file_name_fasta + "  -p blastn -d " + args.blast_db[0] + '  -F "" -n T -b 5  -a 16 -v 5 -m 7 -o ' + tmp_file_results_blast + " > /dev/null 2>&1"
                os.system("blastall -i "+tmp_file_name_fasta+"  -p blastn -d "+args.blast_db[0]+'   -F "" -n T -b 5  -a 16 -v 1 -m 7 -o '+tmp_file_results_blast+" > /dev/null 2>&1")

            elif nt == True:
                print "blastall -i " + tmp_file_name_fasta + '  -p blastn -d nt   -F "" -n T -b 5  -a 32 -v 1 -m 7 -o ' + tmp_file_results_blast + " > /dev/null 2>&1"
                os.system("blastall -i " + tmp_file_name_fasta + '  -p blastn -d nt   -F "" -n T -b 5  -a 32 -v 1 -m 7 -o ' + tmp_file_results_blast + " > /dev/null 2>&1")

            #print "Done Blast"
            #if os.stat("tmp_blast_res.xml").st_size == 0:
            #    print "File empty"
            #else:
            #    print "File has data"
            #count=0
            #while os.stat("tmp_blast_res.xml").st_size == 0:
            #    print "Size tmp_blast_res.xml zero file. Iter="+str(count)
            #    print "Waiting for Blast ..."
            #    time.sleep(10);
            #    count +=1
            #    if count == 100:
            #        sys.exit("ERROR: Blast search took more than "+str(count*10)+" s. Aborting")
        except :
            print "WARNING: Could not get BLAST results. Something went wrong ..."
            return results_dict
            #sys.exit("ERROR: Could not run a local instance of BLAST on your system. Try to run without --remote key")


    fp = open(tmp_file_results_blast, "r"); data=fp.read(); fp.close()
    m_pattern = re.compile("Hit"); result = m_pattern.search(data) # check if blast results have any Hits
    #print data, result
    if result:
        fp = open(tmp_file_results_blast, "r")
        records = NCBIXML.read(fp)
        fp.close()
    else:
        print "No Hits in Blast output"
        return results_dict
     #sys.exit() # os.remove("tmp_blast_res.xml")


    for i in range(0, len(records.alignments)):
        #print i
        #print dir(records.alignments[i]); sys.exit()
        #print records.alignments[i].length
        #print records.descriptions[i].score
        #print records.descriptions[i].e
        #print records.descriptions[i].bits
        #print records.alignments[i].hsps[0].align_length
        #records.records.alignments[i]
        #records.alignments[0].hit_id
        #records.alignments[i].accession
        hit_id = records.alignments[0].hit_id
        accession=(re.split("\|", hit_id)[3]) # get accession number for hit full ID (e.g. gi|1384885437|gb|MF858238.1|)
        results_dict[i]= {"blast_hit_title":records.alignments[i].hit_def[0:100],  #limit the field length (otherwise Excel can render incorretly)
                          "blast_hit_accession": accession,
                          "blast_overlap_len": records.alignments[i].hsps[0].align_length,
                          "blast_overlap_start_pos": records.alignments[i].hsps[0].query_start,
                          "blast_overlap_end_pos": records.alignments[i].hsps[0].query_end,
                          "blast_hit_bits": records.descriptions[i].bits,
                          "blast_hit_identity": records.alignments[i].hsps[0].identities
                          }
    #print results_dict
    #print len(records.alignments)
    #sys.exit()
    try:
        os.remove(tmp_file_results_blast); os.remove(tmp_file_name_fasta)  #cleanup
    except StandardError,e:
        print e; pass

    return results_dict

def getBOLDAPIStats(seq, dataframe):
    print "Getting BOLD API taxonomic information"
    import xml.etree.ElementTree as ET
    #print seq
    #seq = "AACATTATACTTTATTTTTGGGGATTGAGCAAGAATAGTAGGAACTTCTTTAAGAGTTTTAGTTGAAACAGGGACAGGAACAGGTTGAACAGTATACCCCCTACTTTCATCTGGAATTGCTCATGCTGGGGCATATGTAGATTTAGCAATTTTTTCATTACATTTAGCAGGAATCTCTTCTATTTTAGGAGCTGTAAATTTTATTACAACTGTAATTAATATACGAGCTTCAGGTATTACTTTTGATCGAATACCTTTATTTGTTTGATCAGTAGTTATTACAGCAGTATTATTACTTCTTTCTTTACCTGTATTAGCTGGAGCTATTACTATACTATTAACAGATCGAAATTTAAATACTTCATTCTTTGACCCAGCTGGAGGAGGAGATCCTATTCTTTACCAACATTTATTT"
    base_url="http://v4.boldsystems.org/index.php/Ids_xml?db=COX1&sequence="+seq
    print "URL: "+base_url
    try:
        xml_data = requests.get(base_url, timeout=60)
        xml_tree_root = ET.fromstring(xml_data.content)
    except:
        return dataframe
        #print xml_data, type(xml_data), xml_data.content, ET
    #print [el for el in xml_tree_root.iter() if el.tag == "matches"]
    xml_dict={}

    if len([el for el in xml_tree_root.iter() if el.tag == "match"]) == 0: #if number of matches is zero
        print "WARNING: Could not get any matches from BOLD for "+seq
        return dataframe

    for l1 in xml_tree_root:
        for l2 in l1:
            if l2.tag == "ID":
                boldid = l2.text
            if len(xml_dict) == 0:
                xml_dict[boldid]={}
            if l2.tag == "similarity":
                xml_dict[boldid]["similarity"]=l2.text
            for l3 in l2:
                if l3.tag == "url":
                    xml_dict[boldid]["url"]=l3.text
        break #only top hit is needed, skip the others for now
    #print xml_dict

    #html_data = requests.get(xml_dict[boldid]["url"]); #print html_data

    if len(xml_dict)>0:
        dataframe["bold_hit_ID"] = boldid; dataframe["bold_hit_similarity"] = xml_dict[boldid]["similarity"];  dataframe["bold_hit_url"] = xml_dict[boldid]["url"]

    try:
        url = "http://v4.boldsystems.org/index.php/API_Public/combined?ids="+boldid
        response = requests.get(url); tree = ET.fromstring(response.content)
    except:
        return dataframe

    tags = ["phylum", "class", "family", "order", "genus", "species", "bin_uri","nucleotides"]
    for elem in tree.iter():
        current_tag = elem.tag
        #print current_tag
        if any([True if t == current_tag  else False for t in tags]):
            for item in elem.iter():
                if item.tag == "name":
                    dataframe["bold_hit_"+current_tag] = item.text
                elif item.tag == "bin_uri":
                    dataframe["bold_hit_" + current_tag] = item.text
                elif item.tag == "nucleotides":
                    dataframe["bold_hit_" + current_tag] = re.sub('-','',item.text)


    return dataframe

def getTaxonomy(accession):
    record= None; taxid = 0
    try:
        print "Retreiving data via SQLquery"
        #accession = "X53556.1"
        print 'SELECT * FROM accession2taxid WHERE accession="' + str(accession) + '"'
        for row in cur.execute('SELECT * FROM accession2taxid WHERE accession="' + str(accession) + '"'):
            print "Search for the accession# ... : " + str(row)
            print "Accession " + str(row[0]) + " -> Taxid: " + str(row[1])
            taxid = row[1]
        #print "Success"+1
        #record = SeqIO.read(handle, "genbank")
    except StandardError, e:
        print e
        print "Trying to get taxonomy remotely (local query did not work ...)"
        try:
            handle = Entrez.efetch(db="nucleotide", id=accession, rettype="gb", retmode="xml")
            record = Entrez.read(handle)
            handle.close()

        #taxid = 0
        #print "Failed to get taxonomy for "+accession
        #print e
        #try:
        #    print "Retrying to retrieve taxonomy LOCALLY ... "
        #    chunksize = 10 ** 7
        #    file_path = findFilePath("nucl_gb.accession2taxid.gz")
        #    #filename = "/Users/kirill/PycharmProjects/MappedReads2Barcode/nucl_gb.accession2taxid.gz"
        #    for chunk in pd.read_csv(file_path, compression='gzip', chunksize=chunksize, sep="\t"):
        #        taxid = [t for ac, t in zip(chunk["accession.version"].get_values(), chunk["taxid"].get_values()) if ac == accession]
        #        if len(taxid) > 0:
        #            taxid = taxid[0];break
        except StandardError, e:
            taxid = 0
            print type(e).__name__
            pass


    #record = None  #debug - force the 2nd method retreival

    if record == None and taxid == 0:
        #method 2 - recover from taxonomy id error via direct html query
        print "Trying method 2 via curl taxid search"
        #print urllib2.urlopen("https://www.ncbi.nlm.nih.gov/nuccore/KT108316.1")
        try:
            #taxid = accesson2taxid_dict[accession]
            url = "https://www.ncbi.nlm.nih.gov/nuccore/"+accession
            print url
            http_response = Popen(["curl", url], stdout=PIPE).communicate()
            print len(http_response)
            taxid = re.compile('(.*)(ORGANISM=)(\d+)').findall(http_response[0])[1][2]
        except StandardError, e:
            print e
            print("WARNING: Could not get taxonomy id for accession number "+accession+".")
            taxon_dict = {'kingdom': '', 'superkingdom': '', 'family': '', 'superfamily': '', 'subclass': '', 'order': '',  'phylum': '', 'superclass': '', 'species': '', 'class': ''}
            lineage_full = [""] * 10
            return taxon_dict, lineage_full
    elif taxid > 0:
        print "TaxonomyID already obtained " + str(taxid)
    else:
        try:
            for i in range(0,len(record[0]["GBSeq_feature-table"][0]["GBFeature_quals"])):
                parserMatch = re.compile('(taxon:)(\d+)').match(record[0]["GBSeq_feature-table"][0]["GBFeature_quals"][i]["GBQualifier_value"])
                if parserMatch:
                    taxid = parserMatch.group(2)
        except:
            print("WARNING: Could not process record retreived from entrez via method 1 ...")
            taxon_dict = {'kingdom': '', 'superkingdom': '', 'family': '', 'superfamily': '', 'subclass': '','order': '', 'phylum': '', 'superclass': '', 'species': '', 'class': ''}
            lineage_full = [""] * 10
            return taxon_dict, lineage_full

    print "TaxonomyID is "+str(taxid)
    try:
        taxon_dict = NCBITaxa().get_rank(NCBITaxa().get_taxid_translator(NCBITaxa().get_lineage(taxid)).keys());
        taxon_dict = {v: k for k, v in taxon_dict.iteritems()} #invert dictionary
        taxon_dict = {k: NCBITaxa().get_taxid_translator([v]).values()[0] for k, v in taxon_dict.iteritems()}
        lineage_full = [NCBITaxa().get_taxid_translator([i]).values()[0] for i in NCBITaxa().get_lineage(taxid)][2:]
        return taxon_dict, lineage_full
    except StandardError,e:
        print e
        taxon_dict = {'kingdom': '', 'superkingdom': '', 'family': '', 'superfamily': '', 'subclass': '', 'order': '',
                  'phylum': '', 'superclass': '', 'species': '', 'class': ''}
        lineage_full = [""] * 10
        return taxon_dict, lineage_full


    #sys.exit("Done taxid")
    #return record.annotations["taxonomy"]
    #print taxon_dict
    #print taxon_dict.keys(), lineage_full



def progressBar(i, t):
        sys.stdout.write('\tcompleted \r{:2.1f}%'.format(float(i + 1)/float(t) * 100))
        #print time.time()-start_time, i,t
        if i%10 == 0 and i > 0 and time.time()-start_time > 600:
            sys.stdout.write("\n\n****Estimated time for the run completion is {:.4f} h****\n\n".format((abs(time.time()-start_time)/i)*(abs(t-i))/3600))

        sys.stdout.flush()

def readMetaDataFile(path):
    print "Read Meta Data File"
    wb = load_workbook(path, read_only=True);
    ws = wb.active

    title_line = [ws.cell(row=1, column=c).value for c in range(1, 20)]
    search_headers = ["384PlateID", "Processid", "Sampleid", "Coordinate", "Forward MID", "Reverse MID"]
    try:
        c_idx = [title_line.index(name) for name in search_headers]  # extract the columns
    except ValueError, e:
        print str(e)
        sys.exit("ERROR: Make sure the header containts these columns: " + str(search_headers))

    try:
        bold_idx = title_line.index("BOLD_Taxonomy")
        print bold_idx
        if bold_idx > 0 :
            c_idx.append(bold_idx)
            #print c_idx
            #print search_headers
            search_headers.append('BOLD_Taxonomy')
            #print search_headers
            idx_dict = dict(zip(search_headers, c_idx))
    except:
        print "No BOLD_Taxonomy column (BOLD_Taxonomy)"
        idx_dict = dict(zip(search_headers, c_idx))
        pass

    #find column index
    try:
        bold_bin_idx = title_line.index("BOLD_BIN")
        if bold_bin_idx > 0 :
            c_idx.append(bold_bin_idx); search_headers.append('BOLD_BIN'); idx_dict = dict(zip(search_headers, c_idx))
    except:
        print "No BOLD_BIN column (BOLD_Taxonomy)"
        idx_dict = dict(zip(search_headers, c_idx))
        pass

    # raw_data = [d for d in ws.iter_rows("A{}:J{}".format(2,ws.max_row))] # skip the 1st line design as header
    raw_data = [d for d in ws["A{}:Z{}".format(2, ws.max_row)]]  # skip the 1st line design as header
    #print type(raw_data)

    meta_data = {"pid":[],"sid":[], "plateid":[],"crd": [], "BOLD_taxonomy": [], "BOLD_BIN":[]}
    for r in range(0, len(raw_data)):
        plateid = raw_data[r][idx_dict["384PlateID"]].value  # processID
        pid = raw_data[r][idx_dict["Processid"]].value  # processID
        sid = raw_data[r][idx_dict["Sampleid"]].value  # sampleID
        #any([k == "BOLD_Taxonomy" for k in idx_dict.keys()]) == True

        #read data
        if any([k == "BOLD_Taxonomy" for k in idx_dict.keys()]):
            bold_tax = str(raw_data[r][idx_dict["BOLD_Taxonomy"]].value)  # sampleID
            #print bold_tax
        else:
            sys.exit("ERROR: Header 'BOLD_Taxonomy' does not exist in your meta xlsx file. Add it meta data!")

        if any([k == "BOLD_BIN" for k in idx_dict.keys()]):
            bold_bin = str(raw_data[r][idx_dict["BOLD_BIN"]].value)  # sampleID
        else:
            sys.exit("ERROR: Header 'BOLD_BIN' does not exist in your meta xlsx file ("+args.metafile[0]+"). Add it to meta data!")

        if sid:
            sid = re.sub('\.Pac[B|b]io', '', sid)
        else:
            sid = ""
        if pid is not None or sid is not None:
            meta_data["pid"].append(pid)
            meta_data["sid"].append(sid)
            meta_data["plateid"].append(plateid)
            meta_data["crd"].append(raw_data[r][idx_dict["Coordinate"]].value)
            meta_data["BOLD_taxonomy"].append(bold_tax)
            meta_data["BOLD_BIN"].append(bold_bin)
    #print meta_data["bold_taxonomy"]
    #sys.exit()
    return meta_data

def initializeRowDictionary(plateid, pid, sid, nreads, meta):
    dictionary = {     "Plate": plateid,
                       "ProcessID": pid, "SampleID": sid, "Nreads": nreads, "numberOfBarcodesPerSampleID": "",
                       "fasta_file": "", "Cons_BC": "",
                       "blast_OTU_name": "", "blast_accession": "", "genbank_taxonomy": "", "genbank_tax_domain": "",
                       "genbank_tax_phylum": "", "genbank_tax_class": "",
                       "genbank_tax_order": "", "genbank_tax_suborder": "", "genbank_tax_family": "", "genbank_tax_genus": "",
                       "genbank_tax_species": "",
                       "blast_overlap_len": "", "blast_hit_bits": "", "blast_min_hit_bits": "",
                       "blast_hit_identity": "", "blast_hit_identity_percent": "",
                       "bold_hit_ID": "","bold_hit_similarity":"","bold_hit_url": "",
                       "bold_hit_phylum":"", "bold_hit_class":"", "bold_hit_family":"", "bold_hit_order":"", "bold_hit_genus":"",
                       "bold_hit_species":"", "bold_hit_bin_uri":"", "bold_hit_nucleotides":"",
                       "stop_codon_present":"No", "multipleBarcodesPerSampleID": "No", "BOLD_BIN_mismatch":"Yes"
                       }

    if len(meta) > 0:
        meta_idx = [i for i in range(0, len(meta["sid"])) if meta["sid"][i] == sid]
        if meta_idx != []:
            print meta_idx
            meta_idx = meta_idx[0]
            dictionary["BOLD_taxonomy"] = meta["BOLD_taxonomy"][meta_idx] #assign predicted taxonomy
            dictionary["BOLD_BIN"]=meta["BOLD_BIN"][meta_idx]  #assign predicted BINs
            dictionary["BOLD_taxonomy_mismatch"] = "Yes" #initialize by default to "Yes"
        else:
            #print meta["sid"][0:10]
            sys.exit("ERROR: Could not find match  for " + sid)

    return  dictionary

def assignBLASTresults(df, blast_stats):
    #print blast_stats[0]
    df["blast_OTU_name"] = blast_stats[0]["blast_hit_title"]
    df["blast_accession"] = blast_stats[0]["blast_hit_accession"]
    df["blast_overlap_len"] = blast_stats[0]["blast_overlap_len"]
    df["blast_hit_bits"] = blast_stats[0]["blast_hit_bits"]
    df["blast_min_hit_bits"] = min([blast_stats[i]["blast_hit_bits"] for i in range(0, len(blast_stats))])
    df["blast_hit_identity"] = blast_stats[0]["blast_hit_identity"]
    df["blast_hit_identity_percent"] = float(final_df["blast_hit_identity"]) / float(final_df["blast_overlap_len"])
    #sys.exit("SDD")
    return df

def assignTAXONOMYresults(df, lineage_list, taxonomy_dict):
    #fill in blank keys with spaces
    for k in ["phylum", "superclass", "order", "family", "genus", "species"]:
        if taxonomy_dict.get(k) == None:
            taxonomy_dict[k] = ""

    #in case taxonomy is empty, return initail data frame as is
    if len(lineage_list) == 0 and len(taxonomy_dict) == 0:
        return df

    #print len(taxonomy_dict), taxonomy_dict
    name_pairs = [("genbank_tax_domain","superkingdom"), ("genbank_tax_phylum","phylum"), ("genbank_tax_class","superclass"), ("genbank_tax_order","order"),
                  ("genbank_tax_suborder", "suborder"), ("genbank_tax_family","family"), ("genbank_tax_genus","genus"), ("genbank_tax_species","species")]

    if len(taxonomy_dict) > 0 :
        df["genbank_taxonomy"] = ";".join(lineage_list)
        for i, j in name_pairs:
            #print taxonomy_dict[j]
            try:
                df[i] = taxonomy_dict[j]
            except:
                df[i] = ""
                pass


        #df["genbank_tax_domain"] = taxonomy_dict["superkingdom"]
        #df["genbank_tax_phylum"] = taxonomy_dict["phylum"]
        #df["genbank_tax_class"] = taxonomy_dict["superclass"]
        #df["genbank_tax_order"] = taxonomy_dict["order"]
        #df["genbank_tax_suborder"] = taxonomy_dict["suborder"]
        #df["genbank_tax_family"] = taxonomy_dict["family"]
        #df["genbank_tax_genus"] = taxonomy_dict["genus"]
        #df["genbank_tax_species"] = taxonomy_dict["species"]
    return df

def writeResultsRowOut(csv_writer, final_df, header_keys):
    csv_writer.writerow([final_df[k] for k in header_keys])

def checkBOLDSubmitTaxonomy(df, seq):
    if args.fastmode == True:
        print("Fast Mode. BOLDAPI not run!")
        df["BOLD_taxonomy"] = "FASTMODE(NOT RUN)"
        return df
    print "Checking taxonomy ..."
    print "BOLD submitted taxonomy: "+df["BOLD_taxonomy"]
    #print df["genbank_taxonomy"]
    #print re.compile("Hymenoptera").match(df["genbank_taxonomy"])
    #print re.compile(df["BOLD_taxonomy"]+";").findall(df["genbank_taxonomy"])

#if len(re.compile(df["BOLD_taxonomy"]+";").findall(df["genbank_taxonomy"])) > 0 :
    #print "NCBI Taxonomy matched"
    #df["BOLD_taxonomy_mismatch"] = "No"
#else:
   # print "NCBI Taxonomy did not match, fall-back to BOLD"
    print "Always running BOLD since taxonomy in NCBI can be wrong ..."
    df = getBOLDAPIStats(seq, df)
    bold_headers = ["bold_hit_class", "bold_hit_family", "bold_hit_order", "bold_hit_genus", "bold_hit_species"]
    bold_lineage = ";".join([df[k] for k in bold_headers])
    match_result = re.compile("(.*)"+df["BOLD_taxonomy"]+"(.*)").match(bold_lineage)

    if str(type(match_result)) == "<type '_sre.SRE_Match'>":
        print "BOLD Taxonomy matched"
        df["BOLD_taxonomy_mismatch"] = "No"
    else:
        try:
            print "Trying 3rd way of getting taxonomy - Local Blast with full nt database"
            results = getBlastStatsPerSeq(seq, blast_remote_flag=False, nt=True)
            #print results
            df = assignBLASTresults(df, results)
            #print df["blast_accession"]
            taxonomy_dict, lineage_list = getTaxonomy(df["blast_accession"])
            df = assignTAXONOMYresults(df, lineage_list, taxonomy_dict)
            #match_result = re.compile("(.*)" + df["BOLD_taxonomy"] + "(.*)").match(df["genbank_taxonomy"])
            if len(re.compile(df["BOLD_taxonomy"]+";").findall(df["genbank_taxonomy"])) > 0:
                print "BOLD Taxonomy matched"
                df["BOLD_taxonomy_mismatch"] = "No"
            else:
                print "BOLD Taxonomy did NOT match"
                df["BOLD_taxonomy_mismatch"] = "Yes"
        except StandardError,e:
            print "BOLD taxonomy did NOT match due to an unknown error"
            print e
            df["BOLD_taxonomy_mismatch"] = "Yes"
            #sys.exit()


    #print taxonomy_dict
    return df

def checkBOLDSubmitBIN(final_df):
    if final_df["BOLD_BIN"] == final_df["bold_hit_bin_uri"]:
        final_df["BOLD_BIN_mismatch"] = "No"
    return  final_df



def checkStopCodons(df):
    #print "Checking for stop codons presence"
    nstop_codons = {}; min_stopcodons = 0
    #print "checkStopCodons(df) "+df["Cons_BC"]
    for frame in range(0, 3):
        for ntable in [1,2,3,4,5,9,10,11,12,13,15,16,21,22,23,24,25,26]: #removed  The Alternative Flatworm Mitochondrial Code (table 14) # The Ciliate, Dasycladacean and Hexamita Nuclear Code (table 6)
            #print "Frame" + str(frame) + "table "+ str(ntable)
            seqObject = Seq(df["Cons_BC"][frame:], Alphabet.DNAAlphabet())
            seq_translated = str(seqObject.translate(table=ntable))
            stop_codon_idx = [m.start() for m in re.compile('\*').finditer(seq_translated)]
            nstop_codons[frame] = {"seq": seq_translated, "nstop": len(stop_codon_idx), "positions": stop_codon_idx}

            min_stopcodons = min([(nstop_codons[k]["nstop"]) for k in nstop_codons.keys()])
            #print "checkStopCodons(df): " + str(min_stopcodons)
            if min_stopcodons == 0:
                df["stop_codon_present"] = "No"
                break
            min_stopcodons = min([(nstop_codons[k]["nstop"]) for k in nstop_codons.keys()])
            #print "checkStopCodons(df): " + str(min_stopcodons) +"  --->  "+seq_translated

    if min_stopcodons > 0:
        df["stop_codon_present"] = "Yes"

    return df

def setOverallRedFlag(df): #plus the contaminant check
    #if args.bold_bin_match == True:
    headers = ["BOLD_taxonomy_mismatch", "multipleBarcodesPerSampleID", "BOLD_BIN_mismatch"]
    if all([df[h] == "No" for h in headers[0:2]]) or all([df[h] == "No" for h in headers[1:]]): #even if tax or BINs mismatch, do not flag
        df["Red_flagged"] = "No"
    else:
        df["Red_flagged"] = "Yes"
    #else:
    #    headers = ["BOLD_taxonomy_mismatch", "multipleBarcodesPerSampleID"]
    #    if all([df[h] == "No" for h in headers]):
    #        df["Red_flagged"] = "No"
    #    else:
    #        df["Red_flagged"] = "Yes"

    # "stop_codon_present"  - do not set overall flag to yes if stop-codon is present (collections request)

    #QC3 check for contamination if contamination class is found in the well
    contaminant_list = ["Bacteria", "Chordata", "Nematoda", "Nemertea", "Proteobacteria"]

    #if any([item for item in contaminant_list if df["bold_hit_phylum"] == item or df["genbank_tax_domain"] == item]):
    #print any([ str(type(re.compile("(.*)"+item+"(.*)").match(df["bold_hit_phylum"],re.IGNORECASE))) == "<type '_sre.SRE_Match'>" or \
    #          str(type(re.compile("(.*)" + item + ";(.*)").match(df["genbank_taxonomy",re.IGNORECASE]))) == "<type '_sre.SRE_Match'>" for item in contaminant_list ])

    #if  any([ str(type(re.compile(item+"(.*)").match(df["bold_hit_phylum"],re.IGNORECASE))) == "<type '_sre.SRE_Match'>" or \
    #          str(type(re.compile("(.*)" + item + ";(.*)").match(df["genbank_taxonomy",re.IGNORECASE]))) == "<type '_sre.SRE_Match'>" for item in contaminant_list ]):
    #    df["Red_flagged"] = "CONTAMINANT"

    #Check for the contaminant words in both NCBI and BOLD taxonomies
    if any([len(re.compile(item+"(.*)").findall(df["bold_hit_phylum"],re.IGNORECASE))>0 for item in contaminant_list] +
            [len(re.compile(item + ";(.*)").findall(df["genbank_taxonomy"], re.IGNORECASE)) > 0 for item in contaminant_list]):
        df["Red_flagged"] = "CONTAMINANT"

    #assing contaminant flag based on supplied sequences
    contaminant_seq_dict={}
    #df["Cons_BC"]="GACACTGTACATTATTTTTTCCATATTAGCTGGAATTATTGGTGGATTATTATCGGTGATTATTCGCACTCAGCTAATGCACATTAATATACTTAACAATAACTATCAATTATATAACGTAATGGTTACAGGGCATGCGTTGATAATGGTATTTTTTATGATAATGCCAGCCCTCATGGGAGGATTTGGTAACTGGTTTGTACCTCTCATGATTGGCGCACCAGATATGGCGTTTCCTCGTATGAATAATTTGAGTTTCTGGTTATTAGTGTCATCTTTTATTATGCTCATTCTCTCAGTGTTTATTGGTGAAGGTCCAGGTACAGGTTGGACTTTATATCCACCTTTATCACAGGTAATGTCCCATCCAAGTGCAGGAGTTGACATTGCTATACTTGCACTTCATGTCGCTGGTATGTCGTCAATTGTTGGGGCGATCAACTTTATAGTTACTATATTTAACATGCGCACAAAAGGCATGTCATTAACTAAGATGCCACTGTTTGTTTGGTCTGTCTTGTTAACAGCATTTATGTTGATTGTTGCCTTACCAGTGCTTGCCGGTGCTATAACTATGCTTCTTACTGATCGCAATATTGGTACTCCCTTTTTTGATCCTGCCGGTGGCGGCGATCCTGTGTTATTTCAACATCTATTT"
    try:
        with open(findFilePath("Sequel_contaminated.fasta"), "rU") as fp:
            for record in SeqIO.parse(fp, "fasta"):
                contaminant_seq_dict[record.description] = str((record.seq).rstrip("-"))
        #print contaminant_seq_dict
        for contam_seq in contaminant_seq_dict.values():
            if len(re.compile(contam_seq).findall(df["Cons_BC"])) >= 1:
                df["Red_flagged"] = "CONTAMINANT"; break
            elif len(re.compile(df["Cons_BC"]).findall(contam_seq)) >= 1:
                df["Red_flagged"] = "CONTAMINANT";break
    except StandardError as e:
        print e
        print "WARNING: Could not find FASTA file (Sequel_contaminated.fasta) with the contamination sequences ... "
        pass #if it fails, no big deal, just move on

    #print df["Red_flagged"]
    #sys.exit("CP1")
    #print [len(re.compile(item+";(.*)").findall(x,re.IGNORECASE)) for item in contaminant_list]

    return df

def findFilePath(filename):
    Files = {}
    #filename = "nucl_gb.accession2taxid.gz" #debug
    try:
        cwd = os.path.abspath(check_output(["which", "reads2barcode.py"]).rstrip())
    except:
        sys.exit("\nERROR in findFilePath(): could not find location of the reads2barcode.py script on your PC. Please make sure \"which reads2barcode.py\" returns path\n"
                 "ERROR in findFilePath(): Also make sure reads2barcode.py can be executed (e.g. run chmod a+x reads2barcode.py)\n"
                 "ERROR in findFilePath():  Alternatively define -path2db parameter with complete path the database accession2taxid.db\n")
    root_dir = re.compile("(.*MappedReads2Barcode)").match(cwd).group(1)
    #root_dir = "/"
    print "Root:"+root_dir
    for path, dirs, files in os.walk(root_dir):
        #print path
        #files = os.listdir(path)
        #print files
        for file in files:
            if file == filename and os.stat(path+"/"+file).st_size != 0:
                    Files[file] = os.path.abspath(path+"/"+file)
    try:
        return Files[filename]
    except:
        print "ERROR: Could not find the "+filename

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Mapped reads to a consenus Barcode')
    parser.add_argument('-i', '--input', nargs=1, dest="input_file", default=None, metavar='', type=str, help = 'Input path to a TXT file with MAPPED READS')
    parser.add_argument('-p', nargs=1, dest="primers_file", default=None, metavar='', type=str, help='Input path to a FASTA file with FWD and REV primers')
    parser.add_argument('-cap3', '--cap3_output', nargs=1, dest="cap3_output", default=None, metavar='', type=str, help='Output path where to store temp CAP3 results TXT file')
    parser.add_argument('-blast-remote', nargs="?", action="store", dest="blast_remote", default=False, metavar='', type=bool, help='Use remote BLAST (i.e. NCBI servers). Otherwise local instance')
    parser.add_argument('-blastdb', nargs=1, dest="blast_db", default=False, metavar='', type=str, help='Local blast database name (e.g. nt)')
    parser.add_argument('-metafile', nargs=1, dest="metafile", default=None, metavar='', type=str, help='Meta data EXCEL file')
    #parser.add_argument('-majority-vote', nargs="?", dest="majorityvote", default=False, metavar='', type=bool, help='Final barcode selection by majority vote? Otherwise output all consensus contigs')
    parser.add_argument('-o', '--output', nargs=1, dest="output_file", default=None, metavar='', type=str, help='Output Results TXT file')
    parser.add_argument('--notrimming', dest="notrimming", action="store_true", help='Complete annotation for blank cases after a run')
    parser.add_argument('--fastmode', dest="fastmode", action="store_true", help='Fast annotation mode (NCBI resource only)')
    parser.add_argument('-path2db', nargs=1, dest="path2db", default=None, metavar='', type=str, help='Path to Accession2Taxid database (accession2taxid.db)')
    #parser.add_argument('--boldbinmatch', dest="bold_bin_match", action="store_true", help='Match predicted and obtained BOLD bins? (True,False)')

    global args
    args = parser.parse_args()

    print "Connection to accessiong2taxonomy db ..."
    global conn, cur

    if args.path2db == None:
        conn = sqlite3.connect(findFilePath("accession2taxid.db"))
    else:
        print args.path2db[0]
        conn = sqlite3.connect(args.path2db[0])
    #conn = sqlite3.connect("/Users/kirill/PycharmProjects//MappedReads2Barcode/DB/accession2taxid.db") #DEBUG
    cur = conn.cursor()

    if args.cap3_output[0] == args.input_file[0]:
        sys.exit("ERROR: The cap3 and input file path are IDENTICAL! This will cause data loss. Correct -cap3 paramenter to point to other path ")
    #for row in cur.execute('SELECT * FROM accession2taxid WHERE accession="' + "KR808470.1" + '"'):
    #    print "Search for the accession# ... : " + str(row)
    #    print "Accession " + str(row[0]) + " -> Taxid: " + str(row[1])



    print args
    time.sleep(5)

    global start_time
    start_time = time.time()

    #create accession2taxid dictionary from ncbi data (ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz)
    #print "Creating accession2taxonomyid dictionary ..."
    #global accesson2taxid_dict
    #accesson2taxid_dict = {}
    #chunksize = 10 ** 6;

    #read_nlines = 0  # read file in chunks
    #if os.path.isfile("./nucl_gb.accession2taxid.gz") == False:
    #    print "Missing accession# to taxonomy file (nucl_gb.accession2taxid.gz) at path "+os.getcwd()
    #    print "Get this file at ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz"
    #for chunk in pd.read_csv("./nucl_gb.accession2taxid.gz", compression='gzip', chunksize=chunksize, sep="\t"):
    #    # print chunk
    #    read_nlines = read_nlines + len(chunk.index);  print (float(read_nlines)/float(123779123))*100
    #    #print str(sys.getsizeof(acc2taxid_map) / 1e6) + " MB"
    #    if (sys.getsizeof(acc2taxid_map) / 1e6 > 10024):
    #        print ("WARNING: object size is > 10GB!");
    #        break
    #    for ac in zip(chunk["accession.version"].get_values(), chunk["taxid"].get_values()):
    #        if int(ac[1]) > 0:  # filter all accession numbers without assigned taxid (#saves on memory)
    #            accesson2taxid_dict[str(ac[0])] = int(ac[1])

    #check if global env variables is set
    try:
        os.environ['BLASTDB']
    except:
        if args.blast_remote == False:
            sys.exit("ERROR: Global variable BLASTDB is not set")
        else:
            pass
    if args.primers_file == None:
        sys.exit("ERROR: Supply fasta file with fwd and rev primer sequences (option -p)")

    if args.blast_remote == None:
        args.blast_remote = True
        print "Using remote BLAST"
    #if args.majorityvote == None:
    #    args.majorityvote = True
    #else:
    #    args.majorityvote = False

    METADATA={}
    if args.metafile == None:
        sys.exit("Supply Meta Data file containing information on BOLD taxonomy and SampleID, ProcessID mappings")

    METADATA = readMetaDataFile(args.metafile[0]) # ['BOLD_BIN', 'BOLD_taxonomy', 'plateid', 'pid', 'crd', 'sid']
    if len(METADATA["sid"]) == 0 or len(METADATA["pid"]) == 0:
        sys.exit("ERROR: Input METAdata lacks processID or sampleID entries. Can not continue")

    #print METADATA['BOLD_BIN']
    #sys.exit()

    if os.path.exists(args.output_file[0]) == False:
        with open(args.output_file[0], 'w') as fp: #write parameters to final results file
                resume_prev_run = False
                print "NEW RUN ... "
                fp.write("#Input file: {!s}\n#Blast remote: {!s}\n".format(args.input_file[0], args.blast_remote));fp.close(); #try to create a results file (and test connection)
        print args.output_file[0]
    else:
        resume_prev_run = True
        print "RESUMING from previous crashed run as output file exists ..."
        with open(args.output_file[0], 'rU') as fp:  #read the existing results file
            previous_csv_data = csv.reader(fp, delimiter='\t')
            processed_ids = []
            try:
                for row in previous_csv_data:
                    if len(row) > 2:
                        processed_ids.append(row[2])
            except StandardError,e:
                print e
                sys.exit("ERROR: Could not read the results file")



    #print len(set(processed_ids))



    #read the primer sequences for sequence trimming/cleanning
    fwd_primer, rev_primer = getPrimers(args.primers_file[0])
    fwd_primer_regex, rev_primer_regex = getRegexPrimerStrings(fwd_primer,rev_primer)
    print fwd_primer_regex,  rev_primer_regex



    dir_sngl=args.output_file[0][:-4]+"/blacklisted_singletons"; #dir_dub_bc=args.output_file[0][:-4]+"/dubious_barcodes";
    create_dirs(dir_sngl)


    #read input pre-processed data (raw barcodes from previous step in tab-delimited format)
    input_data = read_tab_txt(args.input_file[0]); #list of lists
    #print input_data[0]; sys.exit()

    header = input_data[0]; input_data=input_data[1:len(input_data)];  #skip header


    try:
        bc_col_idx = [i for i in range(0, len(header)) if header[i] == "Barcode"][0];
        sid_col_idx = [i for i in range(0,len(header)) if header[i] == "SampleID"][0];
        pid_col_idx = [i for i in range(0, len(header)) if header[i] == "ProcessID"][0];
        plate_col_idx = [i for i in range(0, len(header)) if header[i] == "PlateID"][0];
        coord_col_idx = [i for i in range(0, len(header)) if header[i] == "Coordinate"][0];
        nreads_col_idx = [i for i in range(0, len(header)) if header[i] == "Nreads"][0];
        descriptiveID_col_idx = [i for i in range(0, len(header)) if header[i] == "DescriptiveID"][0]
    except StandardError,e:
        print e
        print header
        print("ERROR: header name(s) not found. Check header names (ProcessID, SampleID, PlateID, Coordinate,Nreads)")
        raise



    # dictionary of unique sample IDs
    print "Creating dictionary of unique sample IDs ..."
    keys = list(set([data_row[sid_col_idx] for data_row in input_data]))
    unique_sids_dict = collections.OrderedDict([(k,[]) for k in keys])   # unique_sids_dict  -  BIOUG28648-C12: [259, 260]

    total_nlines = len(input_data)
    for ln in range(0,total_nlines): #line number of each sample id in the file
        progressBar(ln,total_nlines)
        m_sid = [sid for sid in unique_sids_dict.keys() if sid == input_data[ln][sid_col_idx]][0]
        unique_sids_dict[m_sid].append(ln)
        #if ln >= 1000:  #!!!!!!!!!!!!! DEBUGGING debuging
        #    print "DEBUG: Reached the set limit of reads"
        #    break


    #create a header ouput file
    out_file_name=args.output_file[0]
    fp_results_out=open(out_file_name, "a")
    csv_writer = csv.writer(fp_results_out, delimiter="\t")
    #fp.write("{}\t{}\t{}\t{}\n".format([row for row in final_df.keys()]))
    header_keys = ["Plate", "ProcessID", "SampleID", "Cons_BC", "Length_BC", "Nreads",
                   "numberOfBarcodesPerSampleID", "Red_flagged", "fasta_file",
                   "blast_OTU_name", "genbank_taxonomy", "genbank_tax_domain", "genbank_tax_phylum",
                   "genbank_tax_class",
                   "genbank_tax_order", "genbank_tax_suborder", "genbank_tax_family", "genbank_tax_genus",
                   "genbank_tax_species",
                   "blast_accession", "blast_overlap_len", "blast_hit_bits", "blast_min_hit_bits", "blast_hit_identity",
                   "blast_hit_identity_percent",
                   "BOLD_taxonomy","BOLD_BIN", "bold_hit_ID", "bold_hit_similarity", "bold_hit_url",
                   "bold_hit_phylum", "bold_hit_class", "bold_hit_family", "bold_hit_order", "bold_hit_genus", "bold_hit_species",
                   "bold_hit_bin_uri", "bold_hit_nucleotides", "stop_codon_present","BOLD_taxonomy_mismatch", "BOLD_BIN_mismatch",
                   "multipleBarcodesPerSampleID"]

    if resume_prev_run == False:
        csv_writer.writerow(header_keys) #write output file header

    total_n_bcs = len([k for k in unique_sids_dict.keys() if len(unique_sids_dict[k]) > 0])

    if resume_prev_run == True:
        print "Total number of samples "+str(total_n_bcs)
        print "Processed already "+str(len(processed_ids)) + " samples ..."
        print len(unique_sids_dict.keys())
        #print [item for item in set(processed_ids) - set(unique_sids_dict.keys()[0:10]) if len(re.compile("BIOUG").findall(item))>0]
        sids_to_process = [item for item in set(unique_sids_dict.keys()) - set(processed_ids)] # if len(re.compile("BIOUG").findall(item))>0] #some sample IDS might not start with BIOG

        print "Still need to process "+str(len(sids_to_process)) + " samples ..."
        #print len(unique_sids_dict.keys())
        unique_sids_dict = {k:unique_sids_dict[k] for k in unique_sids_dict.keys() if k in sids_to_process}
        total_n_bcs = len(sids_to_process)
        #print unique_sids_dict


    #sys.exit()
    #print [{k:unique_sids_dict[k]} for k in unique_sids_dict.keys() if len(unique_sids_dict[k]) > 0]
    final_df={} #will contain all FINAL data

    #assemble reads into a single contig per sampleid
    c_idx=0 #loop counter


    meta_idx = 0 #idex for meta data row location
    for sid in [k for k in unique_sids_dict.keys() if len(unique_sids_dict[k]) > 0]:
        contig_idx_range = []  # indicies range if >1 contigs
        #sid = "BIOUG34716-C05" # DEBUG
        print "Processing "+ sid +"\n"+"*"*50
        print c_idx, total_n_bcs
        progressBar(c_idx, total_n_bcs)
        open(args.cap3_output[0], 'w').close()
        #lns = unique_sids_dict[sid] #lines with this sampleid
        #DEBUG
        #if c_idx == 20:
        #    sys.exit("Done test ...")

        l_idxs=[i for i in range(0,len(input_data)) if input_data[i][sid_col_idx] == sid ] # lines mapping to the sample id

        if len(l_idxs) == 0:
            sys.exit("\nWarning could not find match for the sample ID "+sid+". Check input file "+args.input_file[0])

        #print "Line numbers mapping to sid" + str(l_idxs)
        #print input_data[l_idxs[0]]

        #initialize the final_df row
        plateid = input_data[l_idxs[0]][plate_col_idx]; processid = input_data[l_idxs[0]][pid_col_idx];
        nreads = sum([int(input_data[idx][nreads_col_idx]) for idx in l_idxs]) # automatically sums all the reads in that well (including mismapped)


        fp = open(args.cap3_output[0], "a"); #temp_file
        bc_seqs = {"seqs": [], "fullid": []} #potential barcode sequences


        print "Read raw barcode sequences and try to get single consesnus CCS read (i.e. barcode) ..."
        #print l_idxs
        for ln in l_idxs:
            seq = input_data[ln][bc_col_idx]
            seq = cleanBarcode(fwd_primer_regex, rev_primer_regex,seq) #always try to trim sequences to avoid CAP3 errors. If no trim, then f() will return original seq
            fullid = input_data[ln][descriptiveID_col_idx]
            #print input_data[ln]
            #print len(seq)
            if len(seq) > 0 and seq != "NA":
                fp.write(">"+fullid+"\n"+seq+"\n");
                bc_seqs["seqs"].append(seq); bc_seqs["fullid"].append(fullid)
        fp.close()

        #sys.exit()
        print args.cap3_output[0]

        print "Numer of barcodes "+str(len(bc_seqs["seqs"]))
        #sys.exit("Breakpoint2")

        if len(bc_seqs["seqs"]) >=2:
            devnull = open("out_cap3_screen.txt", 'w')
            try:
                call(["cap3", args.cap3_output[0]], stdout=devnull, stderr=devnull);
            except OSError:
                print("ERROR: CAP3 program is not found! Abroting ... Get it from http://seq.cs.iastate.edu/cap3.html")
                raise

            fp_fasta_contig =open(args.cap3_output[0]+".cap.contigs", "r")

            contig_reads_obj = [read for read in SeqIO.parse(fp_fasta_contig, "fasta")]
            cap3_contigs = {read.name:str(read.seq) for read in contig_reads_obj} # {Contig1: AAA, Contig2: AAA}
            print cap3_contigs
            #sys.exit()
            singlets = open(args.cap3_output[0] + ".cap.singlets", "r").read()
            #print singlets

            print "Number of contigs "+str(len(cap3_contigs))
            if len(cap3_contigs) == 1: #only a SINGLE contig is produced
                print "1 Contig" + sid
                final_df = initializeRowDictionary(plateid, processid, sid, nreads, METADATA)
                #final_df = {'bold_hit_similarity': '', 'ProcessID': 'SPRI205-17', 'blast_hit_identity': '',
                #  'genbank_tax_species': '', 'genbank_tax_class': '', 'bold_hit_phylum': '', 'fasta_file': '',
                #  'genbank_tax_suborder': '', 'genbank_tax_genus': '', 'SampleID': 'BIORTP-053-B03_KCl_SPRI_Ethanol',
                #  'bold_hit_ID': '', 'bold_hit_family': '', 'blast_hit_identity_percent': '', 'genbank_tax_family': '',
                #  'bold_hit_url': '', 'bold_hit_species': '', 'genbank_taxonomy': '', 'bold_hit_class': '',
                #  'bold_hit_nucleotides': '', 'bold_hit_bin_uri': '', 'blast_hit_bits': '', 'bold_hit_order': '',
                #  'Plate': 'BL-106EFL', 'BOLD_taxonomy_mismatch': '', 'blast_OTU_name': '', 'genbank_tax_phylum': '',
                #  'BOLD_BIN': 'BOLD:AAN8137', 'blast_accession': '', 'blast_overlap_len': '', 'stop_codon_present': 'No',
                #  'blast_min_hit_bits': '', 'Cons_BC': '', 'BOLD_taxonomy': 'Vespula vidua', 'bold_hit_genus': '',
                #  'multipleBarcodesPerSampleID': 'No', 'genbank_tax_order': '', 'Nreads': 170,
                #  'numberOfBarcodesPerSampleID': '', 'genbank_tax_domain': ''}
                final_df["Red_flagged"] = "No"
                #seq_trim = cleanBarcode(fwd_primer_regex, rev_primer_regex, cap3_contigs[cap3_contigs.keys()[0]]) #just in case we w
                final_df["Cons_BC"] = cap3_contigs[cap3_contigs.keys()[0]]
                final_df["Length_BC"] = len(cap3_contigs[cap3_contigs.keys()[0]])
                blast_stats = getBlastStatsPerSeq(final_df["Cons_BC"], args.blast_remote)

                if blast_stats:
                    final_df = assignBLASTresults(final_df,blast_stats)
                    taxonomy_dict, lineage_list = getTaxonomy(final_df["blast_accession"])
                    final_df = assignTAXONOMYresults(final_df, lineage_list, taxonomy_dict)

                final_df["multipleBarcodesPerSampleID"] = "No"
                final_df["numberOfBarcodesPerSampleID"] = 1

                #QC filters
                final_df = checkStopCodons(final_df)  # check QC1: stop codons
                final_df = checkBOLDSubmitTaxonomy(final_df, cap3_contigs[cap3_contigs.keys()[0]])  # check QC2: Taxonomy both NCBI and BOLD
                final_df = checkBOLDSubmitBIN(final_df)
                final_df = setOverallRedFlag(final_df)  # checkQC3 and final setting

                writeResultsRowOut(csv_writer, final_df, header_keys)
                #sys.exit("CP1")


            elif len(cap3_contigs) > 1: #more than one contig produced
                print ">1 Contigs " + sid #always set Yes if >1 contig
                nseqs_per_contig = ace_file_parser(args, cap3_contigs.values())
                print nseqs_per_contig

                #print sid
                #sys.exit("D")
                #final_df[c_idx]["Cons_BC"] = "NA"
                #if not all([i == max(nseqs_per_contig.values()) for i in nseqs_per_contig.values()]): #the majority vote
                    #if args.majorityvote == True:
                        #print [i == max(nseqs_per_contig.values()) for i in nseqs_per_contig]
                        #max_key = [k for k in nseqs_per_contig.keys() if nseqs_per_contig[k] == max(nseqs_per_contig.values())][0]
                        #final_df[c_idx]["Nreads"] = nseqs_per_contig[max_key]
                        #print max_key; print re.match('(.*)(\d+)', max_key).group(2)
                        #final_df[c_idx]["Cons_BC"] = cap3_contigs[int(re.match('(.*)(\d+)', max_key).group(2))-1];
                        #final_df[c_idx]["Length_BC"] = len(cap3_contigs[int(re.match('(.*)(\d+)', max_key).group(2))-1])
                        #final_df[c_idx]["Red_flagged"] = "No"
                    #elif args.majorityvote == False:
                #c_idx_start = c_idx #the first contig overall index

                for k in cap3_contigs.keys():

                    #if len(final_df[c_idx]["Cons_BC"]) > 0:
                    #c_idx += 1 #increment only if there slot is occupied
                    #final_df[c_idx] = initializeRowDictionary(plateid,processid,sid,nreads, METADATA)
                    #final_df[c_idx]["Cons_BC"] = contig
                    #final_df[c_idx]["Length_BC"] = len(contig)
                    #final_df[c_idx]["Red_flagged"] = "Yes"

                    final_df = initializeRowDictionary(plateid, processid, sid, nseqs_per_contig[k], METADATA)

                    final_df["Red_flagged"] = "Yes" # automatically red-flag is there are multiple contigs (collections request). Makes easier filter problematic sampleids
                    #seq_trim = cleanBarcode(fwd_primer_regex, rev_primer_regex, cap3_contigs[k])
                    final_df["Cons_BC"] = cap3_contigs[k]
                    final_df["Length_BC"] = len(cap3_contigs[k])
                    blast_stats = getBlastStatsPerSeq(final_df["Cons_BC"], args.blast_remote)

                    if blast_stats:
                        final_df = assignBLASTresults(final_df, blast_stats)
                        taxonomy_dict, lineage_list = getTaxonomy(final_df["blast_accession"])
                        final_df = assignTAXONOMYresults(final_df, lineage_list, taxonomy_dict)

                    final_df["multipleBarcodesPerSampleID"] = "Yes"
                    final_df["numberOfBarcodesPerSampleID"] = len(cap3_contigs)

                    final_df = checkStopCodons(final_df)  # check QC1: stop codons
                    final_df = checkBOLDSubmitTaxonomy(final_df, cap3_contigs[k])  # check QC2: Taxonomy both NCBI and BOLD
                    final_df = checkBOLDSubmitBIN(final_df) #check if BINs coincided
                    final_df = setOverallRedFlag(final_df)  # checkQC3 and final setting + check contaminants
                    writeResultsRowOut(csv_writer, final_df, header_keys)


                    #c_idx_end = c_idx
                    #contig_idx_range = [c_idx_start, c_idx_end]
                    #print c_idx_start, c_idx_end
                    #sys.exit(">1 contig")

               # else: #a 50/50 case
                    #write out all "problematic" barcodes (where the contig supporting counts are identical)
               #     fp = open(dir_dub_bc + "/barcodes_" + sid + ".fasta", "w");
               #     [fp.write(">"+sid+"_consens_barcode"+str(i)+"_nreads"+str(nseqs_per_contig[sorted(nseqs_per_contig.keys())[i]])+"\n"+cap3_contigs[i]+"\n") for i in range(0,len(cap3_contigs))]; fp.close();
               #     final_df[c_idx]["Length_BC"] = "NA"; final_df[c_idx]["Cons_BC"] = "NA"
               #     #[len(bc) for bc in cap3_contigs]
               #     final_df[c_idx]["Red_flagged"] = "Yes";
               #     final_df[c_idx]["fasta_file"]="barcodes_" + sid + ".fasta"

            else: #no contigs produced in CAP3
                print "0 Contigs! "+ sid
                #final_df[c_idx]["Cons_BC"] = bc_seqs;
                #print bc_seqs["seqs"]
                fp = open(dir_sngl + "/reads_" + sid + ".fasta", "w"); fp.close()
                #write all the singlets into a fasta file


                #final_df[c_idx]["Cons_BC"] = "NA"
                #final_df[c_idx]["Length_BC"] = "NA"
                #[len(bc) for bc in bc_seqs]

                for i in range(0,len(bc_seqs["seqs"])):
                    #print bc_seqs["seqs"]
                    #seq = bc_seqs["seqs"][i]
                    #print seq, len(seq)
                    print bc_seqs["fullid"][i]
                    nreads =  re.compile("(.+)(\d+)(reads)").match(bc_seqs["fullid"][i]).group(2)

                    final_df = initializeRowDictionary(plateid, processid, sid, nreads, METADATA)
                    final_df["fasta_file"] = "reads_" + sid + ".fasta"
                    final_df["Red_flagged"] = "Yes"
                    seq = cleanBarcode(fwd_primer_regex, rev_primer_regex, bc_seqs["seqs"][i]) #just in case clean again
                    #print seq_trim

                    final_df["Cons_BC"] = seq
                    final_df["Length_BC"] = len(seq)
                    blast_stats = getBlastStatsPerSeq(final_df["Cons_BC"], args.blast_remote)

                    if blast_stats:
                        final_df = assignBLASTresults(final_df, blast_stats)
                        taxonomy_dict, lineage_list = getTaxonomy(final_df["blast_accession"])
                        final_df = assignTAXONOMYresults(final_df, lineage_list, taxonomy_dict)

                    final_df["multipleBarcodesPerSampleID"] = "Yes"
                    final_df["numberOfBarcodesPerSampleID"] = len(bc_seqs["seqs"])
                    final_df["Red_flagged"] = "Yes"  # automatically red-flag is there are multiple contigs (collections request). Makes easier filter problematic sampleids

                    final_df = checkStopCodons(final_df)  # check QC1: stop codons
                    final_df = checkBOLDSubmitTaxonomy(final_df, seq)  # check QC2: Taxonomy both NCBI and BOLD
                    final_df = checkBOLDSubmitBIN(final_df)
                    final_df = setOverallRedFlag(final_df)  # checkQC3 and final setting --> in this case no need to set a final flag as it is Yes anyways
                    writeResultsRowOut(csv_writer, final_df, header_keys)

                    fp = open(dir_sngl + "/reads_" + sid + ".fasta", "a");
                    fp.write(">" + sid + "_read" + str(i + 1) + "\n" + seq + "\n"); fp.close()
                #sys.exit("BP1")
        else: #if only ONE read per contig
                print "1 read only - no contigs "+sid
                final_df = initializeRowDictionary(plateid, processid, sid, nreads, METADATA)
                final_df["Red_flagged"] = "No"
                #seq_trim = cleanBarcode(fwd_primer_regex, rev_primer_regex, seq)
                final_df["Cons_BC"] = seq
                final_df["Length_BC"] = len(seq)
                blast_stats = getBlastStatsPerSeq(final_df["Cons_BC"], args.blast_remote)

                if blast_stats:
                    final_df = assignBLASTresults(final_df, blast_stats)
                    taxonomy_dict, lineage_list = getTaxonomy(final_df["blast_accession"])
                    final_df = assignTAXONOMYresults(final_df, lineage_list, taxonomy_dict)

                final_df["multipleBarcodesPerSampleID"] = "No"
                final_df["numberOfBarcodesPerSampleID"] = 1
                final_df = checkStopCodons(final_df)  #check QC1: stop codons
                final_df = checkBOLDSubmitTaxonomy(final_df, seq) #check QC2: Taxonomy both NCBI and BOLD
                final_df = checkBOLDSubmitBIN(final_df)
                final_df = setOverallRedFlag(final_df) #checkQC3 and final setting

                writeResultsRowOut(csv_writer, final_df, header_keys)


        c_idx += 1  # overall results index

        #trim sequences
        #if final_df[c_idx]["Cons_BC"] != "NA" and len(final_df[c_idx]["Cons_BC"]) > 0:
            #print final_df[c_idx]["Cons_BC"], len(final_df[c_idx]["Cons_BC"])


        #if args.majorityvote == False and len(cap3_contigs) > 1 and len(contig_idx_range) > 0:
        #    for idx in contig_idx_range:
        #        seq_trim = cleanBarcode(fwd_primer_regex, rev_primer_regex, final_df[idx]["Cons_BC"])
        #        final_df[idx]["Cons_BC"] = seq_trim
        #        final_df[idx]["Length_BC"] = len(seq_trim)





        #get OTU information via BLAST search
        #bc_seq = final_df[c_idx]["Cons_BC"]
        #if len(bc_seq)>0 and bc_seq != "NA":



                #print final_df[c_idx]
                #sys.exit("TaxDone")
        #write out the result
        #row = final_df[c_idx]
        #row["SampleID"] = str(row["SampleID"]) #+ ".PacBio"  # add Pacbio suffix
        #print row; sys.exit()
        #csv_writer.writerow([row[k] for k in header_keys])




        #if c_idx == 20:
        #    break
        #break
        #print final_df[c_idx-1]
        #sys.exit("Breakpoint3")

        #query_sid = "BIOUG29136-D10"
        #if sid == query_sid:
        #    print "Found "+query_sid
        #    sys.exit("Stop")


        #print final_df;

        #print c_idx
        #if c_idx == 100:
        #break



    #print len(final_df.keys())
    #for row in final_df.keys():
    #    row = final_df[row]; row["SampleID"] = str(row["SampleID"])+".PacBio"  #add Pacbio suffix
    #    csv_writer.writerow([row[k] for k in header_keys])
    #fp.close()
    print ("Results stored at "+args.output_file[0])
    print ("Done all steps, This is the End")
    #for i in range(0, len(input_data)):
    #    for sid in unique_sids:
    #        print input_data[i][col_idx]
    fp_results_out.close()

    #parser.add_argument('-m', '--meta-data', nargs=1, dest="meta_file", default=None, metavar='', type=str, help='Input path to META file');
    #parser.add_argument('-mid-fwd', '--mid-fwd-seqs', nargs=1, dest="mid_seq_fwd_file", default=None, metavar='', type=str, help='Input path to forward MID-tag sequences');
