Purpose:
Get the semi-processed mapped reads and output a single barcode and "well profile".
The profile should include information on the OTU/species distribution per well, etc.
This program will synthesize a final well-annotated output


**TODO:**
-[x] Read in tab-delimited txt file containing mapped reads
-[ ] Extract corresponding quality string for each preliminary barcode (from previous steps)
-[x] Test CAP3 consensus sequencing calling
-[ ] Assign taxonomy to reads (e.g. use assign_taxonomy.py or custom blast)
-[ ] Select a single sequence per well, output it in a FASTA format
    - [ ] a single FASTA file with all QC'ed barcoded sequences
    - [ ] a multi FASTA files (one per sampleID) named sampleID.fasta
-[ ] Incorporate previous comments 
    - [x] add sampleID.pacbio
    - [x] remove "service sequences" from final barcodes (e.g. COI-5P primers)
-[ ] add new functions
    -[ ] OTU assignment
    -[x] correction of the read orientation (all reads in 5'->3')
    -[ ] check for duplicated barcodes and remove them
    -[ ] check for human sequences




The main problematic scenarios are
 1. More than 2 consensus reads with the identical number of the original input reads representing a 50/50 tie (dubious_barcodes folder)
 2. Reads that did not produces any consensus read, but only a singleton.  (black_listed_singletons)
Note that sampleID that only had a single reads to begin with, were not subjected to any CAP3 treatment.



#### OTU deteremination via NCBIWWW.qblast (Python Bio.Blast lib)
```
qblast(program, database, sequence, url_base='https://blast.ncbi.nlm.nih.gov/Blast.cgi', 
    auto_format=None, composition_based_statistics=None, db_genetic_code=None, 
    endpoints=None, entrez_query='(none)', expect=10.0, filter=None, gapcosts=None, 
    genetic_code=None, hitlist_size=50, i_thresh=None, layout=None, 
    lcase_mask=None, matrix_name=None, nucl_penalty=None, nucl_reward=None, 
    other_advanced=None, perc_ident=None, phi_pattern=None, query_file=None, 
    query_believe_defline=None, query_from=None, query_to=None, searchsp_eff=None, 
    service=None, threshold=None, ungapped_alignment=None, word_size=None, alignments=500, 
    alignment_view=None, descriptions=500, entrez_links_new_window=None, 
    expect_low=None, expect_high=None, format_entrez_query=None, format_object=None, 
    format_type='XML', ncbi_gi=None, results_file=None, show_overview=None, megablast=None)
    
    Do a BLAST search using the QBLAST server at NCBI or a cloud service provider.


    Some useful parameters:

     - program        blastn, blastp, blastx, tblastn, or tblastx (lower case)
     - sequence       The sequence to search.
     - ncbi_gi        TRUE/FALSE whether to give 'gi' identifier.
     - descriptions   Number of descriptions to show.  Def 500.
     - alignments     Number of alignments to show.  Def 500.
     - expect         An expect value cutoff.  Def 10.0.
     - matrix_name    Specify an alt. matrix (PAM30, PAM70, BLOSUM80, BLOSUM45).
     - filter         "none" turns off filtering.  Default no filtering
     - format_type    "HTML", "Text", "ASN.1", or "XML".  Def. "XML".
     - entrez_query   Entrez query to limit Blast search
     - hitlist_size   Number of hits to return. Default 50
     - megablast      TRUE/FALSE whether to use MEga BLAST algorithm (blastn only)
     - service        plain, psi, phi, rpsblast, megablast (lower case)

```