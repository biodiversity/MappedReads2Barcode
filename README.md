# MappedReads2FinalBarcode
This essential script outputs taxonomically annotated QC checked potential consensus barcodes. Several barcodes are possible per `SampleID`. 
This allows to select the most suitable sequence for submission to BOLD (by Collections Team). The taxonomic annotation is
based upon BLASTN and BOLD top hit result. The taxonomic lineage, amount of the overlap between sequences, accessionID is provided.

The QC checks consist of:
1. if inferred taxonomy matches to previously assigned one by Collections Team; 
2. if inferred barcode has no stop codons using all available DNA alphabets
3. if more than a single contig per sampleID is present (automatically red-flagged to be checked by Collections Team)
4. if common contaminant (e.g. Bacteria, Chordata, Nematoda, Nemertea, Proteobacteria) is present Note: some NCBI hits are bacterial contaminant. 
 Jayme promised me to send list of IDs to blacklist

## Requirements
1. `cap3` is installed in your system and is accessible from any directory (check your $PATH variable)
2.  global variable`BLASTDB` points to the BLAST database directory 
    1.  E.g. `/Users/kirill/mysoft/blast-2.2.22/db/`
    1. `$BLASTDB=/mnt/Files/tbraukmann/db`
3. `blastall` is installed contaning both [`nt`](ftp://ftp.ncbi.nlm.nih.gov/blast/db/) and `coi` databases (see [`coi`](./DB/COI_NCBI) resource)
4. (optional) database in SQLite format containing a table allowing to execute query to covert from NCBI accession ID to taxonomy ID.
E.g. Execute query `SELECT * FROM accession2taxid WHERE accession=...`. In addition index the database for quicker data retrieval.
See [`Create-accession2taxid-database`](#create-accession2taxid-database) section

## Input/Output
Takes demux reads from PacBioCCS2ReadsDemultiplexer and annotation pipeline. After inspection by collections a final set 
of barcodes is uploaded to BOLD repository.

|Input                                               | Output                                        |
|----------------------------------------------------|-----------------------------------------------|
| single tab-txt file containing mapped barcodes     | a final set of barcodes (one per sample id) |                  |


## Usage

MappedReads2FinalBarcode can be run at the command-line as follows:

```
  reaads2barcode.py [PARAMETERS]
```

The main parameters specify input and output paths

|Parameter    | Description                                                                                |Example     |
|-------------|--------------------------------------------------------------------------------------------|------------|
| -i          | Input **path** to a tab-delimited TXT file with MAPPED READS stored under 'Barcode' column |-i input.txt| 
| -p          | Input path to a FASTA file (e.g. *primers.fasta*) with FWD and REV primers*                |-p primers.fasta|
| --cap3       | Output path (e.g. /TMP) where to store temp CAP3 temporary results                         |-cap3 /path/tmp/cap3_outscreen.txt |
| --blast-remote | Perform remote BLASTN search using NCBI web resource (instead of local search)          |--blast-remote=True (default=False)|
| -blastdb      |  Name of BLAST db to use (e.g. nt,coi,etc.)                                              |-blastdb coi |
| -metafile     |  Abs path to the Excel meta file with all run meta data (e.g. MID tags, SampleIDs, Taxonomy  |-metafile meta.xlsx |
| --notrimming | Trim barcode sequences from the technical sequences (e.g. primers, adapters). Omit key if trimming is wanted    |--notrimming |
| -path2db      | (optional) Abs path to database with accession2taxid conversion table. Otherwise will be found automatically  | -path2db |
| -o          | Output path to the results tab-delimited txt file                                          |-o output.txt |


### Notes
1. **primers.fasta* contains degenrative primer sequences that will be used to trim/clip the query sequence (thus, no MID tags will remain)
```
>C_LepFolF
RKTCAACMAATCATAAAGATATTGG
>C_LepFolR
TAAACTTCWGGRTGWCCAAAAAATCA
```

2. Preparation of the Excel meta file:
    ##### Option 1
    1. if taxonomy is available add it by including `BOLD_Taxonomy` column. 
       1. First create 'Idx' column
       
        ```
        =MATCH(A2,Sheet1!$A$1:$A$9900,0
        ```
       2. Second, add `BOLD_Taxonomy` folder
       ```
       =IF(LEN(INDEX(Sheet1!E:E,K2,1))>0,INDEX(Sheet1!E:E,K2,0),IF(LEN(INDEX(Sheet1!D:D,K2,1))>0,INDEX(Sheet1!D:D,K2,1),INDEX(Sheet1!C:C,K2,0)))
       ```
       
    ##### Option 2
    1. Locate [meta_data_BOLD_Taxonomy_column_prep.R](./AUX_SCRIPTS/meta_data_BOLD_Taxonomy_column_prep.R) script
    2. Provide 	paths to the XLSX Excel file with taxonomy info via `-t` or `--taxonomyfile` keys
	3. Provide 	paths to the XLSX Excel file with plate records and meta file info via `-m` or `--metafile` keys
	4. New file will be written to `*_modded.xlsx` file in the execution directory
    
3. Include database `accession2taxid.db` file in `reads2barcode.py` directory (E.g. at ./DB directory)
This file contains NCBI nucleotide DB accession two taxonomy ID conversions. 
Please keep this folder **inside** the `MappedReads2Barcode` folder as the `reads2barcode.py` will look for it.


### Example script
1. Option 1: Run the `xfiles-407-v3_t0.txt` file against `coi` NCBI database using local instance of 
`blastn` with no technical sequence trimming. 
Use degenerative primer sequences stored in `primers.fasta` and and meta file `xfiles.xlsx`.

```
in_file="xfiles-407-v3_t0.txt"
in_meta_file="xfiles.xlsx"
echo $in_file

reads2barcode.py -i "/mnt/Files/tbraukmann/Pacbio_CCS_OPP/NGSFT/407/${in_file}" \
--cap3 "/mnt/Files/tbraukmann/Pacbio_CCS_OPP/NGSFT/407/cap3_${in_file}" \
-p "/mnt/Files/tbraukmann/Pacbio_CCS_OPP/NGSFT/407/primers.fasta" \
-blastdb coi \
-metafile "/mnt/Files/tbraukmann/Pacbio_CCS_OPP/NGSFT/${in_meta_file}" \
-o "/mnt/Files/tbraukmann/Pacbio_CCS_OPP/NGSFT/407/Barcoded_${in_file}" \
--notrimming \
```

2. Option 2: Run remote blast and with sequence trimming

```
    python ${root_dir}/reads2barcode.py -i "${in_file}" \
    ...
    -blast-remote=True \
    ...
```


### Create accession2taxid database
1. Download the most recent taxonomy file `nucl_gb.accession2taxid.gz` from NCBI at [ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/](ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/)
2. Run python script [`createSQLiteDB.py`](./AUX_SCRIPTS/cleanBarcodes.py) to build custom database (edit script to add full paths)
3. Index database based on accession number 
```
    CREATE UNIQUE INDEX idx ON accession2taxid (accession)
```