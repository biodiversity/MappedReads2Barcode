#!/usr/bin/perl
#$f1 is fasta file containing reads
#$f2 needs ot be an integer greater than or equal to 0.  This will determine the similarity for clustered sequences. I.e. 3 would be for 97%, 2 for 98%.  Program recommends not exceeding 4.
($f1,$f2)=@ARGV;
# awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' < A01_CCS.fasta > tmp1.fa
system("fasta_formatter -i $f1 -o tmp1.fa");
system("fastx_trimmer -f 55 -i tmp1.fa -o end.fa");
system("fastx_trimmer -t 55 -i end.fa -o out.fa");
system("fastx_collapser -v -i out.fa -o out3.fa");
system("perl -pe 's:-:;size=:g' out3.fa > out4.fa");
system("usearch -cluster_otus out4.fa -otus out5.fa -uparseout $f1.out -otu_radius_pct $f2 -relabel OTU_ -sizein -sizeout");
system("usearch -sortbysize out5.fa -fastaout out6.fa -minsize 1");
system("perl -pe 's:;::g' out6.fa > out7.fa");
system("perl -pe 's:size=:;:g' out7.fa > $f1.OTU.fasta");
system("rm out.fa end.fa");
system("rm out3.fa tmp1.fa");
system("rm out4.fa out5.fa out6.fa");
system("rm out7.fa $f1.out");