#!/usr/bin/env python

# FlaggedReads2Barcode.py
# input: annotated barcodes that were processed by Reads2Barcode.py
# output: outputs results file with annotated blasted reads



import sys, os
from subprocess import call #system calls
import linecache   #to read specific line of the file
import argparse
from collections import OrderedDict
from openpyxl import load_workbook   # pip install openpyxl
import csv
import re #exact string matching
import tre #fuzzy regex matching
#from subprocess import call
import collections
from Bio.Seq import Seq
from Bio import SeqIO, Entrez
Entrez.email = 'kbessono@uoguelph.ca'
#import copy
import time
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
import random, string #for random number generator
from ete3 import NCBITaxa #get taxonomy given taxID  (e.g. print  NCBITaxa().get_rank(NCBITaxa().get_taxid_translator(NCBITaxa().get_lineage(1666733)).keys()))
def ParseIputFile(file):
    data=open(file,"r").readlines()
    flag_seq_rows_idx=[]


    rf_idx_col=0; header_line=""
    for ln, line in enumerate(data):

        line = line.split("\t")

        try:
            if rf_idx_col == 0:
                rf_idx_col = [i for i in range(0,len(line)) if line[i] == "Red_flagged"][0]
                header_line = [l.rstrip() for l in line]
        except:
            pass


        # print idx_col
        if rf_idx_col > 0:
            if line[rf_idx_col] == "Yes":
                flag_seq_rows_idx.append([ln, line[rf_idx_col]])

    rf_extracted_data = OrderedDict()
    for h in header_line:
        rf_extracted_data[h] = []
    #rf_extracted_data = OrderedDict({h: [] for h in header_line})

    for i in range(0, len(flag_seq_rows_idx)):
        data_line = data[flag_seq_rows_idx[i][0]].split()
        for h, d in zip(header_line, data_line):
            rf_extracted_data[h].append(d)


    return rf_extracted_data

def getSequences(data):
    print data['fasta_file']
    print args.input_file[0]
    try:
        root_directory = re.compile("(.+)([\/\\\])(.+)").match(args.input_file[0]).group(1)+"/"
        file = re.compile("(.+)([\/\\\])(.+)").match(args.input_file[0]).group(3)
        fasta_dir = re.compile('(.+)(\.)(.+)').match(file).group(1)+"/"
    except:
        sys.exit("ERROR: Could not find the directory with fasta files. Check folder names")

    singletons_dir = os.path.abspath(root_directory+fasta_dir+"blacklisted_singletons")+"/"
    singletons_files =  os.listdir(singletons_dir)
    dubs_dir = os.path.abspath(root_directory + fasta_dir + "dubious_barcodes")+"/"
    dubs_files =  os.listdir(dubs_dir)

    fasta_file_path_map = {}
    for f in singletons_files:
        fasta_file_path_map[f] = singletons_dir+f
    for f in dubs_files:
        fasta_file_path_map[f] = dubs_dir  + f

    print len(fasta_file_path_map.keys())

    data_seqs= {}
    for f in data['fasta_file']:
        #print fasta_file_path_map[f]
        fp=open(fasta_file_path_map[f], "r")
        for record in SeqIO.parse(fp, "fasta"):
            data_seqs[f]={}
            data_seqs[f][record.id] = str(record.seq)


    return data_seqs

def getBLASTdata(master_data_frame):
    print "Hi"

if __name__ == '__main__':

    global args
    parser = argparse.ArgumentParser(description='Annotate dubious reads and decide on final barcode')
    parser.add_argument('-i', '--input', nargs=1, dest="input_file", default=None, metavar='', type=str, help = 'Input path to a TXT file with annotated barcodes')
    parser.add_argument('-o', '--output', nargs=1, dest="output_file", default=None, metavar='', type=str, help='Output final results TXT file')

    args = parser.parse_args()

    master_data_frame = ParseIputFile(args.input_file[0])
    seqs= getSequences(master_data_frame)


    for i in range(0,len(master_data_frame)):
        for k in master_data_frame.keys():
            print master_data_frame[k]




    #define which row are flagged
    #read all the sequences (consensus or barcodes)
    #annotate via BLAST
    #annotate via BOLD
    print "Results stored in "+args.output_file[0]
