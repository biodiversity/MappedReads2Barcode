#Determine the source of contamination of the sequence in the query well
#Determine distance(s) to contamination wells

import Levenshtein, re, tre, openpyxl, datetime,time,subprocess
import pandas as pd, os, csv
from Bio import SeqIO
from Bio.Align.Applications import ClustalwCommandline
import matplotlib.pyplot as plt
from Bio.Seq import Seq
import pickle

def cleanBarcode(fwd_primer_regex,rev_primer_regex,seq):  # orients reads in 5'-3' orientation and clips the COI-5P primers

    idx_start, idx_end = fuzzy_str_regex_match(fwd_primer_regex,seq)
    if idx_start != None and idx_end != None:
        cleaned_seq = seq[idx_end:]
    else:
        seq_rev = str(Seq(seq).reverse_complement())  # reverse complement the read
        idx_start, idx_end = fuzzy_str_regex_match(fwd_primer_regex, seq_rev)
        #print idx_start, idx_end
        if idx_start != None and idx_end != None:
            cleaned_seq = seq_rev[idx_end:]
        else:
            cleaned_seq = seq  #all tries failed, no clipping will be done at this end

    #REVERSE clip
    idx_start, idx_end = fuzzy_str_regex_match(rev_primer_regex, str(Seq(cleaned_seq).reverse_complement()))
    #print idx_start, idx_end
    if idx_start != None and idx_end != None:
        cleaned_seq = cleaned_seq[0:-idx_end]
    else:
        #print idx_start, idx_end
        idx_start, idx_end = fuzzy_str_regex_match(rev_primer_regex, cleaned_seq)
        if idx_start != None and idx_end != None:
            cleaned_seq = cleaned_seq[0:idx_start]
        else:
            print "INFO: Reverse clipping methods failed"

    return cleaned_seq

def fuzzy_str_regex_match(in_pattern, in_str):
    tp = tre.compile(in_pattern)
    fz = tre.Fuzzyness(maxerr=5)
    m = tp.search(in_str, fz)
    if m:
        return m.groups()[0][0], m.groups()[0][1]
    else:
        return None,None


def read_fasta(fp):
    ref_seq_dict = {}
    for record in SeqIO.parse(fp, "fasta"):
            ref_seq_dict[record.id] = str((record.seq).rstrip("-"))
    return ref_seq_dict


def distance2reference(query_seq,ref_seq):


    fp_tmp =open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/seq"+amplicon+".fasta","w"); fp_tmp.write(">query\n"+query_seq+"\n>ref\n"+ref_seq+"\n"); fp_tmp.close();

    #print os.getcwd()


    #clustalomega_cline = ClustalOmegaCommandline(infile=in_file, outfile=out_file, verbose=True, auto=True)
    #os.system("clustalw2 "+ "/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/seq"+amplicon+".fasta")

    from Bio import AlignIO

    #print dir(alignment)
    #print alignment.get_all_seqs()




    #print start_idx,end_idx, overlap_len, largest_seq[start_idx:end_idx]

    #run raw clustalw2
    #print seq1, seq2
    #stdout = subprocess.check_output("clustalw2 seq"+amplicon+".fasta | grep 'Group 1: Sequences:   2      Score:'", shell=True)
    #print stdout
    #print re.compile('(.*)(Score:)(\d+)').match(stdout).group(3)
    #print smallest_seq

    #dist_obj = Levenshtein.editops(largest_seq[start_idx:end_idx], seq2)
    #distance = (float(len(dist_obj)) / float(overlap_len)) * 100 #number of edit operations normalized by the overlap length

    #ref match at the start
    tp = tre.compile(ref_seq[0:20])
    fz = tre.Fuzzyness(maxerr=2)
    m = tp.search(query_seq, fz)

    len_ref_seq = len(ref_seq); len_query_seq = len(query_seq)

    if m:
        #print "Worked 1st"
        start_idx = m.groups()[0][0]; end_idx = start_idx+min(len_ref_seq,len_query_seq)

        overlap = query_seq[start_idx:start_idx+end_idx]
    else:
        #print "Worked 2nd"
        #maybe match is not at the ref start but end. try to find match in the ref seq
        tp_ref = tre.compile(query_seq[0:20])
        m_ref = tp_ref.search(ref_seq, fz)
        if m_ref == None:
            return 1e3
        ref_seq = ref_seq[m_ref.groups()[0][0]:]
        tp = tre.compile(ref_seq[0:20])
        m = tp.search(query_seq, fz)
        if m:
            start_idx = m.groups()[0][0];
            end_idx = start_idx + min(len(ref_seq), len_query_seq)
            overlap = query_seq[start_idx:start_idx + end_idx]
        else:
            return 1e3

    #print ">Q\n"+query_seq
    #print ">R\n"+ref_seq
    #print ">O\n"+overlap
    #print len(overlap)

    #cut also ref sequence (since overlap is partial ref_seq)

    ref_seq_cut = ref_seq[0:len(overlap)]
    #print ref_seq_cut

    #else:
    #cline = ClustalwCommandline("clustalw2",infile="/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/seq"+amplicon+".fasta",type='dna')
    #cline()
    #alignment = AlignIO.read("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/seq"+amplicon+".aln", "clustal")
    #for record in alignment:
    #    match_result=re.findall(re.compile('\-{5,}'),str(record.seq))
    #    if match_result:
    #        start_idx = len(match_result[0])
    #        end_idx = len(smallest_seq)
    #        overlap = largest_seq[start_idx:end_idx]




    #dist_obj = Levenshtein.editops(largest_seq, smallest_seq)

    dist_obj = Levenshtein.editops(overlap, ref_seq_cut)
    #distance = (float(len(dist_obj)) / float(len(largest_seq))) * 100
    distance = len(dist_obj)
    #print dist_obj
    #print "Distance: "+str(len(dist_obj))
    #print "Distance normalized: "+str(distance)
    #print len(smallest_seq), len(largest_seq), len(overlap)

    #print len(overlap), len(ref_seq)
    #print distance; exit()

    return distance



def platesOnDeck(source,contam):
    source_int = int(re.compile('(\w+\-)(\d+)').match(source).group(2))
    dest_int = int(re.compile('(\w+\-)(\d+)').match(contam).group(2))

    ranges = {"1":[1,4], "2":[5,8], "3":[9,12],"4":[13,16], "5":[17,20], "6":[21,24]}
    batch_source = [k for k in ranges.keys() if source_int >= ranges[k][0] and source_int <= ranges[k][1]]
    batch_contam= [k for k in ranges.keys() if dest_int >= ranges[k][0] and dest_int <= ranges[k][1]]

    if batch_source == batch_contam:
        ans = "Yes"
    else:
        ans = "No"
    return ans

def physicalDistanceBtwnWells(sourcewell, targetwell):
    print "Physical distance on the plate"
    if 0:
        import networkx as nx
        G = nx.Graph()
        G.add_nodes_from(["A1","A2","A3","B1","B2","B3","C1","C2","C3"])
        G.add_edges_from([("A1","A2"),("A1","B1"),("A1","B2")])
        nx.shortest_path(G, source="A1",target="B2")

    letter2rowDict = {"A":1,"B":2,"C":3,"D":4,"E":5,"F":6, "G":7,"H":8, "I":9,"J":10,"K":11,"L":12,"M":13,"N":14,"O":15,"P":16}
    #transform to number notation
    sourcewell_row = int(letter2rowDict[sourcewell[0]]); sourcewell_col = int(sourcewell[1:])
    targetwell_row = int(letter2rowDict[targetwell[0]]); targetwell_col = int(targetwell[1:])

    #horiz distance

    col_dist = abs(targetwell_col - sourcewell_col)


    # vertical distance

    row_dist = abs(targetwell_row - sourcewell_row)


    #print sourcewell_row, sourcewell_col, targetwell_row, targetwell_col, row_dist, col_dist
    return col_dist+row_dist




if __name__=='__main__':
    amplicon="658"
    fp_results = open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/Barcoded/Barcoded_xfiles-"+amplicon+"-v3_t0.txt","r")
    results_df = pd.read_table(fp_results,sep="\t", header=2); fp_results.close()

    fp_references = open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/references_sanger/xfile_reference.fas","r")
    ref_seqs = read_fasta(fp_references); n_refs = len(ref_seqs.keys()); fp_references.close()

    fp_meta = open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/xfiles.xlsx", "r")
    meta_df = openpyxl.load_workbook(fp_meta).active; fp_meta.close()


    results_out_path = "/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/summary_contamination_"+amplicon+"-v3.txt"
    fp_out = open(results_out_path, "w")
    fp_out.write("Query_SampleID\tQuery_Plate\tQuery_well\tMatch_ProcessID\tMatch_Plate\tMatch_well\tMatch_ProcessID\tMatch_SampleID\tMatch_Nreads\tMatch_Species\tMatch_Order\tMatch_SubOrder\tSamePlate?\tOnDeckTogether?\tDistOnPlate(hops)\tAbsEditDistance(Overlap)\n")
    fp_out.close()
    distance_dict = {}

    #blank_rows = [rown for rown in range(1,results_df.shape[0]) if len(re.compile("BLANK.*").findall(results_df.loc[rown,"SampleID"])) > 0 ]
    #exit()

    for rown in range(1,results_df.shape[0]):
    #for rown in blank_rows:
        # DEBUG find a particular row
        #rown = [i for i in range(1, results_df.shape[0]) if results_df.loc[i, "SampleID"] == "BIOUG31269-A10"][0]
        #print rown
        #print results_df.loc[rown,]

        query_sampleid = results_df.loc[rown, "SampleID"]
        row_query_idx = [i for i in range(1,meta_df.max_row) if  meta_df.cell(column=1,row=i).value == query_sampleid]

        if len(row_query_idx) > 0:
            row_query_idx = row_query_idx[0]
            query_plate = results_df.loc[rown, "Plate"]; query_well = meta_df.cell(column=4,row=row_query_idx).value
            print "Processing "+str(rown)+" out of "+str(results_df.shape[0])+" query "+query_sampleid+" from plate "+ query_plate +" well "+ query_well + " amplicon "+amplicon
            query_seq = results_df.loc[rown, "Cons_BC"]

            #clean query sequence from adapters
            query_seq = cleanBarcode("RKTCAACMAATCATAAAGATATTGG", "TAAACTTCWGGRTGWCCAAAAAATCA", query_seq)
            #print query_sampleid, query_seq
            ts = time.time()
            #print datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
            distance_dict[query_sampleid] = {}

            #print results_df.loc[rown,]
            #fp_out = open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/out"+query_sampleid+".txt", "w")
            #fp_out.write(results_df.loc[rown, "ProcessID"] + "\t" + results_df.loc[rown, "SampleID"] + "\t" + query_seq + "\n\n");
            #fp_out.write("RefSeqName\tDistance\n")
            #fp_out.close()

            #exit("Error")

            c=0; print query_sampleid
            for key in ref_seqs.keys():
                #print key
                #try:
                #    ref_sid = re.compile('(.*)(BIOUG.*)').match(key).group(2)
                #except:
                #    pass
                #if ref_sid == "BIOUG31269-A10":
                distance_dict[query_sampleid][key] = distance2reference(query_seq, ref_seqs[key])
                c=c+1

            min_distance = min([distance_dict[query_sampleid][k] for k in distance_dict[query_sampleid].keys()] )
            print "Min distance "+str(min_distance)
            #print sorted([distance_dict[query_sampleid][k] for k in distance_dict[query_sampleid].keys()])
            #print distance_dict[query_sampleid].keys()
            #print distance_dict[query_sampleid]["GMBZB425-16|Formicidae|BIOUG31316-D09"]

            #write pw distance list for each query
            #fp_out = open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/out_"+amplicon+"_"+query_sampleid+".txt", "a")
            #for k in distance_dict[query_sampleid].keys():
            #    fp_out.write(str(k)+"\t"+str(distance_dict[query_sampleid][k])+"\n")

            #print distance_dict[query_sampleid].keys()
            n, bins, patches = plt.hist([distance_dict[query_sampleid][k] for k in distance_dict[query_sampleid].keys()], edgecolor='black', bins=100)
            idxs = [idx for idx in range(0, len(patches)) if patches[idx].get_x() > min_distance-0.5 and patches[idx].get_x() < min_distance+0.5] #'get_transformed_clip_path_and_affine', 'get_url', 'get_verts', 'get_visible', 'get_width', 'get_window_extent', 'get_x', 'get_xy', 'get_y', 'get_zorder', 'have_units', 'hitlist',
            plt.title("Distance distrubution for "+query_sampleid); plt.xlabel('LV edit distance (absolute)'); plt.ylabel('Frequency')
            #print idxs, [patches[idx] for idx in idxs], min_distance

            for idx in idxs:
                patches[idx].set_fc('r')
            #plt.show()
            #show distance distribution to all references
            plt.savefig("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/Figures/Fig"+query_sampleid+".png",dpi=300); plt.close()

            #find the source well
            #print min_distance, distance_dict[query_sampleid].values()
            id_string = [k for k in distance_dict[query_sampleid].keys() if distance_dict[query_sampleid][k] == min_distance][0]
            print id_string
            ref_processid = re.compile('(.*\-\d+\|{1,1})').match(id_string).group(1)[:-1]
            #print "Reference: "+ref_processid
            row_idx = [i for i in range(1, meta_df.max_row) if meta_df.cell(column=2, row=i).value == ref_processid][0]
            ref_plate = str(meta_df.cell(column=3, row=row_idx).value)
            ref_plate_coord = str(meta_df.cell(column=4,row=row_idx).value)

            print "reference with min distance to the query with ProcessID " + ref_processid
            #print results_df[results_df["ProcessID"] == ref_processid]

            fp_out =open(results_out_path,"a")
            idx = [r for r in range(1, results_df.shape[0]) if results_df.loc[r,"ProcessID"] == ref_processid]  # only return 1 item even if there are >1 entries per processid

            if len(idx) >0:
                idx = idx[0]
                ref_sampleid = str(results_df.loc[idx,"SampleID"])
                ref_sample_nreads = str(results_df.loc[idx,"Nreads"])

                ref_species = str(results_df.loc[idx,"blast_OTU_name"]) #.to_string(header=False,index=False)
                ref_order = str(results_df.loc[idx,"genbank_tax_order"])
                ref_suborder = str(results_df.loc[idx,"genbank_tax_suborder"])
            else:
                ref_sampleid = ""; ref_sample_nreads = ""; ref_species = ""; ref_order = ""; ref_suborder = "";
            #print ref_order
            #exit()

            #source and contamination same plate?
            sample_plate_flag = ""
            if query_plate == ref_plate:
                sample_plate_flag = "Yes"
            else:
                sample_plate_flag = "No"

            #plates were on the same deck?
            on_deck_flag = platesOnDeck(query_plate, ref_plate)


            if sample_plate_flag == "Yes":
                physDist = physicalDistanceBtwnWells(query_well,ref_plate_coord)
            else:
                physDist = "NA"

            if min_distance != 100:
                fp_out.write(query_sampleid+"\t"+query_plate+"\t"+query_well+"\t"+ref_processid+"\t"+ref_plate+"\t"+ref_plate_coord+"\t"+ref_processid+"\t"+
                                 ref_sampleid+"\t"+ref_sample_nreads+"\t"+ref_species+"\t"+ref_order+
                                 "\t"+ref_suborder+"\t"+sample_plate_flag+"\t"+on_deck_flag+"\t"+str(physDist)+"\t"+str(min_distance)+"\n"); fp_out.close()
            else:
                fp_out.write(query_sampleid + "\t" + query_plate + "\t" + query_well + "\t \t \t \t \t \t \t \t \t \t \t \t \t \n");fp_out.close()

            #find meta data about processid
            row_idx = [i for i in range(1,meta_df.max_row) if  meta_df.cell(column=2,row=i).value == ref_processid][0]
            #for i in (3,4):
            #    print meta_df.cell(column=i,row=row_idx).value



    #save distance object
    pickle.dump(distance_dict, open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/distanceObject_"+amplicon+".pickle","w"))
    #pickle.dump(results_df,open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/results_dfObject_"+amplicon+".pickle", "w"))

    #restore
    distance_dict = pickle.load(open("/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/NGSFT/distanceObject.pickle","r"))
    #--------------------------------
    #print results_df.loc[0,'Plate']
    #print results_df.loc[0,'Cons_BC']
    #print results_df.shape[0]
    #print ref_seqs.keys()[0:5]
    #print ref_seqs["JIAAJ231-16|Scelioninae|BIOUG29070-C03"]


