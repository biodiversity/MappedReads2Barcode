import csv, sys
import tre #fuzzy regex matching
from Bio.Seq import Seq

def fuzzy_str_regex_match(in_pattern, in_str):
    tp = tre.compile(in_pattern)
    fz = tre.Fuzzyness(maxerr=2)
    m = tp.search(in_str, fz)
    if m:
        return m.groups()[0][0], m.groups()[0][1]
    else:
        return None,None



def cleanBarcode(fwd_primer_regex,rev_primer_regex,seq, data_row):  # orients reads in 5'-3' orientation and clips the COI-5P primers
    d_dict = {"W": "[A|T]", "S": "[C|G]", "M": "[A|C]", "K": "[G|T]", "R": "[A|G]", "Y": "[C|T]",
              "B": "[C|G|T]", "D": "[A|G|T]", "H": "[A|C|T]", "V": "[A|C|G]", "N": "[A|C|G|T]", "A": "A", "T": "T",
              "G": "G", "C": "C"}  # degenerative dictionary

    # forward
    fwd_primer_regex = "".join([d_dict[l] for l in fwd_primer_regex])  # forward match
    #print fwd_primer_regex

    # reverse
    rev_primer_regex = "".join([d_dict[l] for l in rev_primer_regex])
    #print fwd_primer_regex, rev_primer_regex; cleaned_seq=""
    #print args.notrimming

    ##seq = "CTGCGTGCTCTACGACGCAGTCGAACATGTAGCTGACTCAGGTCACAGTCAACAAATCATAAAGATATTGGAACTTTATATTTTATTTTCGGCGCTTGGTCAGGAATAATTGGAACATCTTTAAGTATATTAATTCGTGCAGAATTAGGGCACCCGGGAAATTTAATTGGGAATGATCAAATTTATAATGTAATTGTAACTGCTCACGCTTTTATTATGATTTTTTTTATAGTAATACCAATTATAATTGGCGGGTTTGGTAATTGATTGGTCCCTCTGATATTGAGGGCACCTGATATAGCTTTCCCACGAATAAACAATATAAGATTTTGAATACTCCCCCCCTCAATCAGGCTCCTTTTGATTAGTAGCTTAGTTGAAAATGGGGCAGGAACCGGGTGAACAGTTTACCCCCCTCTTTTCAGCCAACATTGCCTCATGCCGGCGGATCAGTTGATTTAGCAATTTTTTCCCTTCATTTAGCTGGAATTTCTTCTATTTTAGGGGCTGTTAATTTTATTACAACTATTATTAACATACGTTCTAATGGAATTACATTTGATCGAATACCTTTATTTGTATGGTCGGTATTTATTACCGCTATTTTATTATTATTATCTTTACCAGTATTGGCTGGGGCAATTACTATACTTTTAACAGATCGAAATATTAATACTTCATTTTTTGACCCTGCCGGAGGGGGAGACCCAATTTTATACCAACATCTATTTTGATTTTTTGGACATCCTGAAGTTTACTACGATGTGATGCTTGCACAAGTGATCCAGCTAGTCGATGACAGCCTACC"
    #FORWARD end clip
    idx_start, idx_end = fuzzy_str_regex_match(fwd_primer_regex,seq)
    if idx_start != None and idx_end != None:
        cleaned_seq = seq[idx_end:]
    else:
        seq_rev = str(Seq(seq).reverse_complement())  # reverse complement the read
        idx_start, idx_end = fuzzy_str_regex_match(fwd_primer_regex, seq_rev)
        #print idx_start, idx_end
        if idx_start != None and idx_end != None:
            cleaned_seq = seq_rev[idx_end:]
        else:
            cleaned_seq = seq  #all tries failed, no clipping will be done at this end
    #print  cleaned_seq
    #print idx_start, idx_end
    #REVERSE end clip
    idx_start, idx_end = fuzzy_str_regex_match(rev_primer_regex, str(Seq(cleaned_seq).reverse_complement()))

    if idx_start != None and idx_end != None:
        cleaned_seq = cleaned_seq[0:-idx_end]
    else:
        #print idx_start, idx_end
        idx_start, idx_end = fuzzy_str_regex_match(rev_primer_regex, cleaned_seq)
        if idx_start != None and idx_end != None:
            cleaned_seq = cleaned_seq[0:idx_start]
        #else:
            #print "Reverse or All clipping methods failed for " +data_row[2]+ " "+seq + " with length "+str(len(seq)) + " bp"
            #cleaned_seq = seq

            #blast_res = getBlastStatsPerSeq(cleaned_seq, False)
            #if blast_res:
            #    print len(cleaned_seq)
            #    cleaned_seq = cleaned_seq[blast_res[0]["blast_overlap_start_pos"]:blast_res[0]["blast_overlap_end_pos"]]  #if all tries fail, return unclipped sequence

    return cleaned_seq


if __name__ == "__main__":
    in_file = "BLOOD_18pM-Cell3_SQL-033_C01_CCS2-995_BLOOD_t0.txt"
    input_file = "/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/RESULTS_OUT/Oct17/TICKS/"+in_file
    out_file = "/Users/kirill/PycharmProjects/PacBioCCS2ReadsDemultiplexer/RESULTS_OUT/Oct17/TICKS/"+in_file+"_trimmed.txt"

    print input_file
    #forward_primer = "[A|G][G|T]TCAAC[A|C]AATCATAAAGATATTGG"
    #reverse_primer = "TAAACTTC[A|T]GG[A|G]TG[A|T]CCAAAAAATCA"
    #primer_pairs={0: {"fwd":"RKTCAACMAATCATAAAGATATTGG","rev":"TAAACTTCWGGRTGWCCAAAAAATCA", "amplicName":"INSECT"},
    #              1: {"fwd":"TCTCAACCAACCANAANGANATNGG","rev":"TAGACTTCTGGGTGNCCNAANAANCA", "amplicName": "MAMMAL"},
    #              2: {"fwd":"ACYACWRYWATTAAYATAAARCCMC","rev":"TAGACTTCTGGGTGNCCNAANAANCA", "amplicName": "BLOOD"}}
    primer_pairs={0:{"fwd":"ACYACWRYWATTAAYATAAARCCMC","rev":"TAGACTTCTGGGTGNCCNAANAANCA", "amplicName": "BLOOD"}}

    fp=open(out_file, "w"); fp.close()
    csv_writer = csv.writer(open(out_file,"a"), delimiter="\t")
    with open(input_file, 'r') as fp:  # read the existing results file
        previous_csv_data = csv.reader(fp, delimiter='\t')

        header = []; barcodeColNum = None; LenBCNum = None
        for row in previous_csv_data:
            if any([c == "SampleID" for c in row]):
                header=row; csv_writer.writerow(row)
                barcodeColNum = [n for n,col in enumerate(header) if col == "Cons_BC" or col=="Barcode"][0];
                LenBCNum = [n for n,col in enumerate(header) if col == "Length_BC" or col == "Len(bp)"][0]
                AmplicNameNum = [n for n, col in enumerate(header) if col == "AmpliconName" or col == "AmpliconName"][0]
                SampleIDNum = [n for n, col in enumerate(header) if col == "AmpliconName" or col == "SampleID"][0]
                continue #do not continue, just write the header

            if len(header) > 0:
                #clean barcode if only single amplicon is expected
                if len(primer_pairs) == 1:
                    row[barcodeColNum] = cleanBarcode(primer_pairs[0]["fwd"],primer_pairs[0]["rev"], row[barcodeColNum], row)
                #clean barcode if multiple amplicons are expected
                else:
                    for i in range(0, len(primer_pairs)):
                        before_clip_seq_len = len(row[barcodeColNum])
                        trim_seq = cleanBarcode(primer_pairs[i]["fwd"], primer_pairs[i]["rev"], row[barcodeColNum], row)
                        if  len(trim_seq) < before_clip_seq_len:
                            #print "Before " + str(before_clip_seq_len) + " with amplicon name " + str(primer_pairs[i]["amplicName"])
                            #print "After " + str(len(trim_seq)) +" : " + str(trim_seq)
                            row[barcodeColNum] = trim_seq
                            row[AmplicNameNum] = primer_pairs[i]["amplicName"]
                            #
                            # row[SampleIDNum] = row[SampleIDNum]+"_"+primer_pairs[i]["amplicName"] #guarantees unique sampleIDs per amplicon
                            break


                row[LenBCNum] = len(row[barcodeColNum])
                csv_writer.writerow(row)
            #break #DEBUG


    print "Done"
    print "Output "+out_file
    #csv_writer.writerow(out_file)  # write header