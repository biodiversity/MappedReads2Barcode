import pandas as pd
import sys, time
#from klepto.archives import file_archive
#ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/nucl_gb.accession2taxid.gz
def count_lines(file):
    chunksize = 10 ** 5
    count = 0
    for chunk in pd.read_csv(file, compression='gzip', chunksize=chunksize, sep="\t"):
        count = count + len(chunk.index)
    return count


#create dictionary of the accesssion2taxid
#filename = "./nucl_gb.accession2taxid.gz"
filename = "~/Downloads/nucl_gb.accession2taxid.gz"
#nlines = count_lines(filename)
start_time = time.time()
chunksize = 10 ** 6; acc2taxid_map = {}; read_nlines=0 #read file in chunks
for chunk in pd.read_csv(filename, compression='gzip', chunksize=chunksize, sep="\t"):
    #print chunk
    read_nlines = read_nlines+len(chunk.index); #print (float(read_nlines)/float(123779123))*100
    print str(sys.getsizeof(acc2taxid_map)/1e6) + " MB"
    if (sys.getsizeof(acc2taxid_map) / 1e6 > 10024):
        print ("WARNING: object size is > 10GB!"); break
    for ac in zip(chunk["accession.version"].get_values(), chunk["taxid"].get_values()):
        if int(ac[1]) > 0: #filter all accession numbers without assigned taxid (#saves on memory)
            acc2taxid_map[str(ac[0])] = int(ac[1])

print("--- %s seconds ---" % (time.time() - start_time))

#low memory dictionary searches (read file in chunks)
import pandas as pd
accession="KR098613.1"
chunksize = 10 ** 8
filename = "~/Downloads/nucl_gb.accession2taxid.gz"
for chunk in pd.read_csv(filename, compression='gzip', chunksize=chunksize, sep="\t"):
    taxid = [t for ac,t in zip(chunk["accession.version"].get_values(), chunk["taxid"].get_values()) if ac == accession]
    if len(taxid) > 0:
        taxid = taxid[0]
        print accession, taxid
        break

#find nucl_gb.accession2taxid.gz file by scanning directory tree at different hierarchy levels
import os,re
cwd_path = os.getcwd()
#cwd_path="/Users/kirill/PycharmProjects/MappedReads2Barcode/"
levels = (len(re.compile("\/").findall(cwd_path)))-1

def find(query_name, path):
    for root, dirs, files in os.walk(path):
        if query_name in files:
            return os.path.join(root, query_name)


#find in the file by searching the directory tree
result_path=None
for l in xrange(levels-1):
    os.chdir("..")
    result_path = find("nucl_gb.accession2taxid.gz", os.getcwd())
    if result_path != None:
        break

if result_path == None:
    print "Could not locate nucl_gb.accession2taxid.gz"
else:
    print result_path


#reverse translate sequences and find stop codons
from Bio import SeqIO, Entrez, Alphabet, Seq
import re
seq="AATTTTATACTTTATTTTTGGAATATGAGCAGGAATACTTGGTTCCTCAATAAGATTAATTATTCGTATAGAATTAAGAAATCCTGGATTTTTAATTAATAATGATCAAATTTACAATTCTTTTGTAACAGCTCATGCTTTTATTATAATTTTTTTTATAGTTATACCCATTATAATTGGAGGATTTGGAAATTGATTAATTCCTTTAATATTAGGAAGACCTGATATAGCATTTCCTCGAATAAATAATATAAGATTTTGATTATTACCCCCATCTATTTTATTATTAATTTTTAGATCAATTACAAATCAAGGTACTGGAACTGGATGAACAATTTACCCCCCTTTATCTTCAAATATTAATCATGAAGGTTTATCAGTTGATTTAGCAATTTTTTCATTACATATAGCAGGTATATCTTCAATTATAGGAGCTATTAATTTTATTACAACAATTTTTAATATAAAAAATATTAATAAAAAATTTGAACAATTAACTTTATTTACTTGATCAATTAAAATTACAACTATTTTACTTTTATTAGCTGTACCAATTTTAGCTGGTGCTATTACAATACTTTTAACAGATCGAAATTTAAATACTTCATTTTTTGATCCCTCAGGAGGAGGTGATCCAATTTTATATCAACATTTATTT"
nstop_codons = {}
for frame in range(0,3):
    print "Frame"+str(frame)
    seqObject = Seq.Seq(seq[frame:], Alphabet.DNAAlphabet())
    seq_translated = str(seqObject.translate(table="Invertebrate Mitochondrial"))
    stop_codon_idx = [m.start() for m in re.compile('\*').finditer(seq_translated)]
    nstop_codons[frame] = {"seq":seq_translated,"nstop":len(stop_codon_idx),"positions": stop_codon_idx}

min_stopcodons = min([(nstop_codons[k]["nstop"]) for k in nstop_codons.keys()])


# With StopCodon
# BIOUG32793-A05, TATTTTATATTTTATATTTGGAATTTGATCAGGTATTGTAGGTTTATCAATAAGATTAATTATTCGAATGGAATTAAGAGTGGGAGGTAATTTAATTGGAAATGATCAAATTTATAATAGTATTGTTCTGCTCATGCTTTTATTATAATTTTTTTTATAGTTATACCTATTATAATTGGGGGATTTGGTAATTGATTAGTCCCATTAATATTGGGAGGTCCTGATATAGCTTTCCCTCGTATAAATAATATGAGATTTTGATTATTAATCCCTTCTTTATTAATATTAATTTTAAGATCTTTAATTAATATTGGAGTGGGACTGGTTGGACAGTTTATCCTCCGTTATCATTAAATATAAGTCATAGTGGAATATCTGTTGATTTGGCTATTTTTTCTTTACATATTGCGGGAGTTTCTTCTATTATAGGGGCAATAAATTTTATTACTACTATTTTAAATATATGAATAATAAATATTAAAATTGATAAAATGTCTTTATTAATTTGATCAATTTTAATTACTGCTATTTTATTATTATTGTCTTTACCAGTTTTAGCAGGAGCTATTACTATATTATTAACAGATCGTAATTTAAATACAAGATTTTTTGATCCTTCTGGAGGGGGTGACCCAATTTTATATCAACATTTATTT
# BIOUG32793-A10, TATATTATATTTTATTTTTGGGATATGAGCAGGATTTTTAGGATTATCTTTAAGAATATTAATTCGTTTAGAATTAGGATCTTCTGGTTCTTATATTGGAAGAGATCAAATTTATAATTCAATTGTTACAACGCATGCTTTTATTATAATTTTTTTTTTGTTATACCAGTAATAATAGGAGGGTTTGGGAATTATTTAATTCCATTAATATTATTAGTTCCTGATATATCATTTCCACGAATAAATAATATGAGATTTTGATTATTAGTTCCTAGAATTATTTTATTAATTTCTAGAATATTTATTGGATTAGGAACAGGTACTGGATGAACAGTTTATCCTCCTTTATCAATAAATGTATCCCATAGAGGACCTTCAGTTGATTTATCTATTTTTTCTTTGCATTTAGCTGGTGCATCTTCAATTATAGCTTCAATTAATTTTATTACTACTATTTTAAATATAAAAAATTTAAATATAGAAAAAGTAACATTATTTAGTTGATCTATAATATTAACTGCAATTTTATTATTATTATCATTACCAGTATTAGCTGGGGCTATTACTATATTATTATTTGATCGAAATTTAAATACTTCTTTTCTTGATCCTGCGGGAGGAGGAGATCCAATTTTATATCAACATTTATTT
# BIOUG32793-B07
# No StopCodon:
# BIOUG32793-A06, AATTTTATACTTTATTTTTGGAATATGAGCAGGAATACTTGGTTCCTCAATAAGATTAATTATTCGTATAGAATTAAGAAATCCTGGATTTTTAATTAATAATGATCAAATTTACAATTCTTTTGTAACAGCTCATGCTTTTATTATAATTTTTTTTATAGTTATACCCATTATAATTGGAGGATTTGGAAATTGATTAATTCCTTTAATATTAGGAAGACCTGATATAGCATTTCCTCGAATAAATAATATAAGATTTTGATTATTACCCCCATCTATTTTATTATTAATTTTTAGATCAATTACAAATCAAGGTACTGGAACTGGATGAACAATTTACCCCCCTTTATCTTCAAATATTAATCATGAAGGTTTATCAGTTGATTTAGCAATTTTTTCATTACATATAGCAGGTATATCTTCAATTATAGGAGCTATTAATTTTATTACAACAATTTTTAATATAAAAAATATTAATAAAAAATTTGAACAATTAACTTTATTTACTTGATCAATTAAAATTACAACTATTTTACTTTTATTAGCTGTACCAATTTTAGCTGGTGCTATTACAATACTTTTAACAGATCGAAATTTAAATACTTCATTTTTTGATCCCTCAGGAGGAGGTGATCCAATTTTATATCAACATTTATTT
# BIOUG32793-B12,
# BIOUG32793-D09

