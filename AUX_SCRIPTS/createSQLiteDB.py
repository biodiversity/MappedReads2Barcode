import pandas as pd
import sqlite3
from datetime import datetime

if __name__ == '__main__':
    print "Connection to accessiong2taxonomy db ..."
    conn = sqlite3.connect("/Users/kirill/PycharmProjects/MappedReads2Barcode/accession2taxid.db")
    cur = conn.cursor()
    print "Done"


    create_table = False
    # Create table

    if create_table == True:
        cur.execute('''CREATE TABLE accession2taxid (accession text, taxid text)''')

        chunksize = 10 ** 2
        file_path = "/Users/kirill/PycharmProjects/MappedReads2Barcode/nucl_gb.accession2taxid.gz"
        for chunk in pd.read_csv(file_path, compression='gzip', chunksize=chunksize, sep="\t"):
            # Insert a row of data
            for acc,taxid in zip(chunk["accession.version"], chunk["taxid"]):
                cur.execute("INSERT INTO accession2taxid (accession,taxid) VALUES ('"+str(acc)+"','"+str(taxid)+"')")

        conn.commit()

    #index database
    #cur.execute('''CREATE UNIQUE INDEX idx ON accession2taxid (accession);''')

    test_query = True

    if test_query == True:
        #test data retrieval
        print str(datetime.now())
        accession = "X53556.1"
        for row in cur.execute('SELECT * FROM accession2taxid WHERE accession="'+accession+'"'):
            print "Search for the accession X53556.1: "+str(row)
            print "Accession "+str(row[0])+ " -> Taxid: "+str(row[1])
        print "Completed "+str(datetime.now())

        print str(datetime.now())
        accession = "KR808470.1"
        for row in cur.execute('SELECT * FROM accession2taxid WHERE accession="' + accession + '"'):
            print "Search for the accession X53556.1: " + str(row)
            print "Accession " + str(row[0]) + " -> Taxid: " + str(row[1])
        print "Completed " + str(datetime.now())


    conn.close()

