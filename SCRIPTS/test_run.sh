#!/usr/bin/env bash
source activate python_dev

#python /Users/kirill/PycharmProjects/MappedReads2Barcode/reads2barcode.py \
#-i "/Users/kirill/PycharmProjects//PacBio_QC_seqs/RESULTS_OUT/25_bp_window-current/PACBC/99.9_results/PACBC_25bp_t0_99.9.txt" \
#-cap3 "/Users/kirill/PycharmProjects/MappedReads2Barcode/TMP/PACBC_25bp_t0_99.9.txt" \
#-p "/Users/kirill/PycharmProjects/MappedReads2Barcode/SCRIPTS/primers.fasta" \
#-blastdb "coi" \
#-metafile "/Users/kirill/PycharmProjects/PacBio_QC_seqs/DATA/PACBC/PACBC_9120records_MIDtoSampleAssociation_SwapCorrected_EVZ20161221.xlsx" \
#-o "/Users/kirill/PycharmProjects/MappedReads2Barcode/RESULTS/MenloPark/Barcoded_PACBC_25bp_t0_99.9.txt" \
#--notrimming=True \
#--resume=True



root_dir=/Users/kirill/PycharmProjects/MappedReads2Barcode/
in_file="OPP-019_CLepFolF-CLepFolR-9pM_SQL-023_CCS2-999_t0.txt"
in_meta_file="OPP19_LIMS-plate-records_EVZ20170804.xlsx"
echo $in_file

python ${root_dir}/reads2barcode.py -i "/Users/kirill/PycharmProjects/PacBio_QC_seqs/RESULTS_OUT/OntarioProvParks/${in_file}" \
--cap3 "${root_dir}/TMP/${in_file}" \
-p "${root_dir}/SCRIPTS/primers.fasta" \
-blastdb coi \
-metafile "/Users/kirill/PycharmProjects/PacBio_QC_seqs/DATA/OntarioProvParks/${in_meta_file}" \
-o "${root_dir}/RESULTS/OntarioParks/TEST_Barcoded_${in_file}" \
#--notrimming \


# PACBC_9120_barcodedCCS-99 FAILED 15 4/16/2017, 9:02:37 AMadminCCS with Barcoding
# PACBB_9830_barcodedCCS-99 FAILED 14 4/15/2017, 5:18:52 PMadminCCS with Barcoding
# PACBC_9120_barcodedCCS-9999 FAILED 13 4/15/2017, 9:31:28 AMadminCCS with Barcoding
# PACBB_9830_barcodedCCS-9999 FAILED 12 4/14/2017, 10:27:29 PMadminCCS with Barcoding
# PACBC_9120_barcodedCCS-999 FAILED 11 4/14/2017, 3:45:03 PMadminCCS with Barcoding
# PACBB_9830_barcodedCCS-999 FAILED 9 4/14/2017, 8:47:27 AMadminCCS with Barcoding
# PACBB_9123_96by96_try1 FAILED 8 4/12/2017, 10:19:08 AMadminCCS with Barcoding


#last row PBPCR2-57       PACBC2603-16    BIOUG31230-D02 6405 linesec
