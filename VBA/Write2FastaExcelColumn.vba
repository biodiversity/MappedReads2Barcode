Sub extract2Fasta()
Dim myFile As Variant, rng As Range, cellValue As Variant, i As Integer, j As Integer


'Set outputPath = Application.GetSaveAsFilename(_FileFilter:="FASTA Files (*.fasta), *.fasta")
'outputPath = InputBox("Provide output path of the fasta file")
'myFile = Application.DefaultFilePath & "\ExcelFasta.fasta"
'myFile = outputPath & "ExcelFasta.fasta"

myFile = Application.GetSaveAsFilename(InitialFileName:="sequences.fasta")
'myFile = "sequences.fasta"
'myFile = Left(myFile, Len(myFile) - 5)

If Len(myFile) = 0 Then
    MsgBox "No output file path supplied. Aborting"
    End
End If

Debug.Print ActiveWorkbook.ActiveSheet.UsedRange.Rows.Count


'MsgBox "Writting to " & myFile

'Set rng = Selection


Set sampleIDCell = Range("A1:G100").Find("SampleID")

Dim sidColumn As Integer
If sampleIDCell Is Nothing Then
    MsgBox "SampleID column was not found. Check if the sheet has the header"
    End
Else
    sidColumn = CInt(sampleIDCell.Column)
    sidRow = CInt(sampleIDCell.Row)
End If

Debug.Print sidRow
Debug.Print TypeName(sidColumn)
ans = CheckForDuplucates(sidColumn)

Open myFile For Output As #1
For rown = sidRow + 1 To ActiveWorkbook.ActiveSheet.UsedRange.Rows.Count
    cellValueTitle = Cells(rown, sidColumn).Value
    cellValueSeq = Cells(rown, sidColumn + 1).Value
    'Debug.Print TypeName(cellValueSeq)
    If cellValueSeq <> "NA" And Len(cellValueSeq) > 0 Then
        Print #1, ">" & cellValueTitle
        Print #1, cellValueSeq
        counter = counter + 1
    End If
Next rown

Close #1
MsgBox "Done. Successfully exported " & counter & " sequences"
MsgBox "Fasta file written at " & myFile

End Sub


Function IsInArray(stringToBeFound As String, arr As Variant) As Boolean
  IsInArray = (UBound(Filter(arr, stringToBeFound)) > -1)
End Function

Function CheckForDuplucates(sidColumn As Variant)
Debug.Print TypeName(sidColumn)
Dim totalrows As Integer
totalrows = ActiveWorkbook.ActiveSheet.UsedRange.Rows.Count
'Debug.Print TypeName(totalrows)

ReDim SampleIDCollections(1 To totalrows) As String
For r = 1 To totalrows
     SampleIDCollections(r) = Cells(r, sidColumn).Value
     'Debug.Print SampleIDCollections(r)
     'Debug.Print IsInArray(SampleIDCollections(r), SampleIDCollections)
Next r

'Find duplicates
Dim MsgSampleIDReps As String
MsgSampleIDReps = "CHECK THESE IDS:" & vbNewLine
Debug.Print Len(MsgSampleIDReps)
For i = 1 To UBound(SampleIDCollections)
    counter = 0
    For j = 1 To UBound(SampleIDCollections)
        If SampleIDCollections(i) = SampleIDCollections(j) And Len(SampleIDCollections(i)) > 1 Then
            counter = counter + 1
        End If
    Next j
    If counter > 1 Then
        MsgSampleIDReps = MsgSampleIDReps & SampleIDCollections(i) & " x " & counter & vbNewLine
    End If
Next i

Debug.Print Len(MsgSampleIDReps)
If Len(MsgSampleIDReps) > 19 Then
    MsgBox MsgSampleIDReps
    answer = MsgBox("Still want to export duplicated sequences?", vbYesNo, "Make your choice")
    If answer = 7 Then
        MsgBox ("No sequences written. Remove duplicate sampleID entires and try again! Aborting")
    End
    End If
End If

End Function
