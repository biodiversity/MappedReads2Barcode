Sub check4contaminants()

Set flagCell = Range("A1:U100").Find("Red_flagged")
Set phylumCell = Range("A1:U100").Find("genbank_tax_phylum")
Set domainCell = Range("A1:U100").Find("genbank_tax_domain")


Dim flagColumn As Integer, phylColumn As Integer, domainColumn As Integer
If flagCell Is Nothing Then
    MsgBox "RedFlag column was not found. Check if the sheet has the header"
    End
Else
    flagColumn = CInt(flagCell.Column)
    phylColumn = CInt(phylumCell.Column)
    domainColumn = CInt(domainCell.Column)
End If

Debug.Print flagColumn
Debug.Print phylColumn
Debug.Print domainColumn



For rown = sidRow + 1 To ActiveWorkbook.ActiveSheet.UsedRange.Rows.Count
    phylumCellValue = Cells(rown, phylColumn).Value
    domainCell = Cells(rown, domainColumn).Value

    If phylumCellValue = "Chordata" Or phylumCellValue = "Nematoda"  Or phylumCellValue = "Nemertea"  Or domainCell = "Bacteria" Then
        Cells(rown, flagColumn).Value = "CONTAMINANT"
    End If
Next rown
MsgBox "Completed"
End Sub