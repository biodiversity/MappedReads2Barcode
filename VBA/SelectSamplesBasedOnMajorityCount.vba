'Majority vote selection of cells based on number of reads. If sampleid has >1 barcodes, the one with the greates number of reads is selected

Sub selectSamples()
Dim myFile As Variant, rng As Range, cellValue As Variant, i As Integer, j As Integer



Set sampleIDCell = Range("A1:Z10").Find("SampleID")
Set nreadsCell = Range("A1:Z10").Find("Nreads")

Dim sidColumn As Integer
If sampleIDCell Is Nothing Then
    MsgBox "SampleID column was not found. Check if the sheet has the header"
    End
Else
    sidColumn = CInt(sampleIDCell.Column)
    nreadsColumn = CInt(nreadsCell.Column)
    sidRow = CInt(sampleIDCell.Row)
End If

Debug.Print sidRow
Debug.Print TypeName(sidColumn)



For rown = sidRow + 1 To ActiveWorkbook.ActiveSheet.UsedRange.Rows.Count
    CheckForDuplucates(sidColumn)
    cellValueTitle = Cells(rown, sidColumn).Value
    cellValueSeq = Cells(rown, sidColumn + 1).Value
    'Debug.Print TypeName(cellValueSeq)
    If cellValueSeq <> "NA" And Len(cellValueSeq) > 0 Then
        counter = counter + 1
    End If
Next rown


MsgBox "Done. Successfully exported " & counter & " sequences"
MsgBox "Fasta file written at " & myFile

End Sub



Function CheckForDuplucates(sidColumn As Variant)
Debug.Print TypeName(sidColumn)
Dim totalrows As Integer
totalrows = ActiveWorkbook.ActiveSheet.UsedRange.Rows.Count
'Debug.Print TypeName(totalrows)

ReDim SampleIDCollections(1 To totalrows) As String
For r = 1 To totalrows
     SampleIDCollections(r) = Cells(r, sidColumn).Value
     'Debug.Print SampleIDCollections(r)
     'Debug.Print IsInArray(SampleIDCollections(r), SampleIDCollections)
Next r

'Find duplicates
Dim MsgSampleIDReps As String
MsgSampleIDReps = "CHECK THESE IDS:" & vbNewLine
Debug.Print Len(MsgSampleIDReps)
For i = 1 To UBound(SampleIDCollections)
    counter = 0
    For j = 1 To UBound(SampleIDCollections)
        If SampleIDCollections(i) = SampleIDCollections(j) And Len(SampleIDCollections(i)) > 1 Then
            counter = counter + 1
        End If
    Next j
    If counter > 1 Then
        MsgSampleIDReps = MsgSampleIDReps & SampleIDCollections(i) & " x " & counter & vbNewLine
    End If
Next i


End Function

